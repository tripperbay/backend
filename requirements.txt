Django==3.0.2
psycopg2==2.8.4
djangorestframework==3.11.0
channels==2.4.0
channels-redis==2.4.1
django-cors-headers==3.2.1
django-allauth==0.41.0
django-rest-auth==0.9.5
django-activity-stream==0.8.0
django-notifications-hq==1.5.0
Pillow==7.0.0
hashids==1.2.0
bs4==0.0.1
user-agent==0.1.9
