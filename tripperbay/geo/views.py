from rest_framework import generics

from tripperbay.geo import models, serializers


class CitySearchListView(generics.ListAPIView):
    queryset = models.City.objects.all()
    serializer_class = serializers.CitySerializer

    def get_queryset(self):
        """ Returns narrowed down city qs or empty if q query string is not
        provided.
        """
        qs = self.queryset
        q = self.request.query_params.get('q')
        if not q:
            return self.queryset.none()

        return qs.filter(ascii_name__istartswith=q)


class CityDetailView(generics.RetrieveAPIView):
    queryset = models.City.objects.all()
    serializer_class = serializers.CitySerializer


class CountryDetailView(generics.RetrieveAPIView):
    queryset = models.Country.objects.all()
    serializer_class = serializers.CountrySerializer
    lookup_field = 'slug'
