from rest_framework import serializers
from tripperbay.geo import models


class CountrySerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Country
        fields = (
            'id',
            'name',
            'continent_name',
            'image',
            'slug',
        )


class CountryFullSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Country
        fields = '__all__'


class CitySerializer(serializers.ModelSerializer):
    country = CountrySerializer()

    class Meta:
        model = models.City
        fields = (
            'ascii_name',
            'country',
            'display_name',
            'display_name_long',
            'id',
            'image',
            'latitude',
            'longitude',
            'name',
            'slug',
        )


class CityFullSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.City
        fields = '__all__'
