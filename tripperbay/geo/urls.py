from django.conf.urls import url
from tripperbay.geo import views


app_name = 'geo'

urlpatterns = [
    url(r'^city/search/$',
        views.CitySearchListView.as_view(), name='city_search'),
    url(r'^city/detail/(?P<pk>\d+)/$',
        views.CityDetailView.as_view(), name='city_detail'),
    url(r'^country/detail/(?P<slug>[\w-]+)/$',
        views.CountryDetailView.as_view(), name='country_detail'),
]
