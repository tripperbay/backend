from django.contrib.postgres.fields import ArrayField
from django.db import models

from tripperbay.unsplash.models import UnsplashAbstractModel


class Continent(models.Model):
    id = models.CharField(max_length=2, primary_key=True)
    name = models.CharField(max_length=32)

    class Meta:
        ordering = ('name', )

    def __str__(self):
        return f'{self.name}'


class Region(models.Model):
    name = models.CharField(max_length=255)

    class Meta:
        ordering = ('name', )

    def __str__(self):
        return f'{self.name}'


class Country(UnsplashAbstractModel):
    id = models.CharField(max_length=2, primary_key=True)
    area = models.FloatField()
    continent = models.ForeignKey(Continent, models.CASCADE)
    currency_code = models.CharField(max_length=3, null=True, default='')
    currency_name = models.CharField(max_length=255, null=True, default='')
    iso3 = models.CharField(max_length=3)
    iso_numeric = models.CharField(max_length=3)
    name = models.CharField(max_length=255)
    phone = models.CharField(max_length=255, null=True, default='')
    population = models.PositiveIntegerField()

    postal_code_fmt = models.CharField(max_length=255, null=True, default='')
    postal_code_regexp = models.CharField(max_length=255, null=True,
                                          default='')

    region = models.ForeignKey(Region, models.CASCADE)
    tld = models.CharField(max_length=255, null=True, default='')

    slug = models.CharField(max_length=255, default='', blank=True)

    class Meta:
        ordering = ('name', )
        verbose_name_plural = 'Countries'

    def __str__(self):
        return f'{self.name}, {self.region.name}'

    @property
    def continent_name(self):
        return self.continent.name


class CountryDivision(models.Model):
    id = models.CharField(max_length=20, primary_key=True)
    name = models.CharField(max_length=255)
    ascii_name = models.CharField(max_length=255)


class CityManager(models.Manager):
    def get_queryset(self):
        qs = super(CityManager, self)
        return qs.get_queryset().select_related('country', 'country_division')


class City(UnsplashAbstractModel):
    alt_names = ArrayField(
        models.CharField(max_length=200),
        help_text='Alternate names, comma separated, ascii names automatically'
                  ' transliterated, convenience attribute from alternatename '
                  'table'
    )
    ascii_name = models.CharField(
        max_length=200,
        help_text='Name of geographical point in plain ascii characters'
    )
    country = models.ForeignKey(Country, models.CASCADE)
    country_division = models.ForeignKey(CountryDivision, models.CASCADE)
    elevation = models.IntegerField(help_text='In meters')
    latitude = models.FloatField(help_text='In decimal degrees (wgs84)')
    longitude = models.FloatField(help_text='In decimal degrees (wgs84)')
    name = models.CharField(max_length=200,
                            help_text='Name of geographical point (utf8)')
    population = models.IntegerField()
    timezone = models.CharField(max_length=40, help_text='The timezone id')
    is_capital = models.BooleanField(default=False)

    slug = models.CharField(max_length=200, default='', blank=True)

    objects = CityManager()

    class Meta:
        ordering = ('name', '-is_capital', )
        verbose_name_plural = 'Cities'

    def __str__(self):
        return self.display_name_long

    @property
    def display_name(self):
        return f'{self.name}, {self.country.name}'

    @property
    def display_name_long(self):
        return f'{self.name}, {self.country_division.name}, {self.country.name}'  # noqa

    @property
    def country_slug(self):
        return self.country.slug
