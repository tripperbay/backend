from django.apps import AppConfig
# from django.apps import apps as django_apps


class MainConfig(AppConfig):
    name = 'tripperbay.main'

    def ready(self):
        import tripperbay.main.signals  # noqa

        from actstream import registry

        registry.register(
            # django_apps.get_model('auth.User'),
            self.get_model('Account'),
            self.get_model('Group'),
            self.get_model('Invitation'),
            self.get_model('Membership'),
            self.get_model('ProposalStay'),
            self.get_model('ProposalRide'),
        )
