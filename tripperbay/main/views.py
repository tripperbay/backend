import datetime
import pytz
import traceback

from collections import defaultdict

from django.contrib.contenttypes.models import ContentType
from django.db.models import Q, Count
from django.http import Http404

from actstream.models import any_stream, Action
from notifications.signals import notify as notify_signal

from rest_framework import generics, status
from rest_framework.permissions import IsAuthenticated, BasePermission
from rest_framework.response import Response

from tripperbay.main import serializers, models
from tripperbay.main.utils import parse_head, ParseHeadInvalidUrl
from tripperbay.main.logger import logger
from tripperbay.geo.models import City, Country
from tripperbay.geo.serializers import (
    CitySerializer, CityFullSerializer,
    CountrySerializer, CountryFullSerializer
)
from tripperbay.unsplash.models import unsplash


class Verb:
    """ Source of Action and Notification verbs

    # actstream.Action used to wrap Group posts
    # notifications.Notification used for user to user notifications

    https://django-activity-stream.readthedocs.io/en/latest/
    https://github.com/django-notifications/django-notifications
    https://activitystrea.ms/specs/json/1.0/#introduction

    """
    accepted = 'accepted'
    approved = 'approved'
    created = 'created'
    declined = 'declined'
    downvoted = 'downvoted'
    joined = 'joined'
    requested = 'requested'
    upvoted = 'upvoted'


def notify(actor, verb, action_object, recipients,
           target=None, serialized=False):
    """ Wrapper to send notification signals
    Returns a list of notification(s)
    """

    _, notifs = notify_signal.send(
        actor,
        verb=verb,
        action_object=action_object,
        target=target,
        recipient=recipients
    )[0]
    if serialized:
        return [serializers.NotificationSerializer(n).data for n in notifs]
    return notifs


class RefIdObject(object):
    """ Works in conjunction with DetailView which is bound to RefIdTimestampModel
    """
    def get_object_by_ref_id(self, model_class=None, ref_id=None):
        model = model_class or self.model
        ref_id = ref_id or self.kwargs['ref_id']
        try:
            obj = model.objects.get(ref_id=ref_id)
        except Exception as exc:
            # XXX
            # theoretically this shouldn't happen, but you never know when
            # the matrix might glitch
            # in case of multiple objects found with same hashid
            logger.error(exc)
            raise Http404
        return obj


class IsSuperUser(BasePermission):
    def has_permission(self, request, view):
        if not request.user.is_authenticated:
            return False
        return request.user.is_superuser


class IsApprovedMember(BasePermission):
    """ Checks the request user account is an approved member of the group

    Applies to:
    - Group objects
    - FK related objects, ie ones with group field on them
    - Action objects
    """
    def has_object_permission(self, request, view, obj):
        if hasattr(obj, 'action_object'):
            if isinstance(obj.action_object, models.Group):
                group = obj.action_object
            elif isinstance(obj.target, models.Group):
                group = obj.target
            else:
                raise Exception
        elif hasattr(obj, 'group'):
            group = obj.group
        else:
            group = obj

        return bool(
            group.get_approved_membership().filter(
                account_id=request.user.account.id
            ).count()
        )


class IsGroupAdmin(BasePermission):
    """ Checks the request user account is the one who created the group

    Applies to objects that have a group field, ie Group FK
    """
    def has_object_permission(self, request, view, obj):
        return request.user.account == obj.group.created_by


class IsObjOwner(BasePermission):
    def has_object_permission(self, request, view, obj):
        """ Checks whether the request user is the object creator
        """
        return obj.created_by == request.user.account


class CanCreateInGroup(BasePermission):
    def has_permission(self, request, view):
        """
        Takes a `group` POST parameter as a Group.id and checks whether
        the request user is an approved member of that group
        """
        qs = request.user.account.get_approved_membership()
        group_ids = qs.values_list('group_id', flat=True)
        return request.data['group'] in group_ids


class IsValidVote(BasePermission):
    def has_permission(self, request, view):
        voted = request.data.get('voted', -1)
        # None would mean a clear vote but its currently disabled
        return voted in (True, False)


class AccountDetailView(generics.GenericAPIView):
    def get(self, request):
        if request.user.is_authenticated:
            request_user = {
                'is_authenticated': True,
                'account': serializers.AccountSerializer(
                    instance=request.user.account
                ).data
            }
        else:
            request_user = {
                'is_authenticated': False,
                'account': None,
            }

        return Response(request_user)


class AccountNotificationListView(generics.ListAPIView):
    serializer_class = serializers.NotificationSerializer
    permission_classes = (IsAuthenticated, )

    def get_queryset(self):
        return self.request.user.notifications.all()


class AccountNotificationsCountUnreadView(generics.GenericAPIView):
    permission_classes = (IsAuthenticated, )

    def get(self, request):
        return Response(self.request.user.notifications.unread().count())


class AccountNotificationsMarkAsReadView(generics.GenericAPIView):
    permission_classes = (IsAuthenticated, )
    http_method_names = ['patch']

    def patch(self, request, **kwargs):
        data = request.user.notifications.mark_all_as_read()
        return Response(data)


class InterestListView(generics.ListAPIView):
    pagination_class = None
    queryset = models.Interest.objects.filter(is_active=True)
    serializer_class = serializers.InterestSerializer


class CurrencyListView(generics.ListAPIView):
    pagination_class = None
    queryset = models.Currency.objects.all()
    serializer_class = serializers.CurrencySerializer


class SavedSearchCreateView(generics.CreateAPIView):
    queryset = models.SavedSearch.objects.all()
    serializer_class = serializers.SavedSearchCreateSerializer

    def perform_create(self, serializer):
        if self.request._request.user.is_authenticated:
            account = self.request._request.user.account
        else:
            account = None

        return serializer.save(
            remote_addr=self.request._request.META['REMOTE_ADDR'],
            http_user_agent=self.request._request.META['HTTP_USER_AGENT'],
            account=account,
        )

    def create(self, request, *a, **kws):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        instance = self.perform_create(serializer)

        return Response({'slug': instance.slug},
                        status=status.HTTP_201_CREATED)


class SavedSearchDetailView(generics.GenericAPIView):
    queryset = models.SavedSearch.objects.all()
    serializer_class = serializers.SavedSearchSerializer

    def get(self, request, **kws):
        # filter by slug in kws
        qs = models.SavedSearch.objects.filter(**kws)
        instance = qs[0]
        data = {
            'object': serializers.SavedSearchSerializer(instance).data,
            'similar_count': qs.count() - 1,
        }
        return Response(data)


class GroupCreateView(generics.CreateAPIView):
    queryset = models.Group.objects.all()
    serializer_class = serializers.GroupCreateSerializer
    permission_classes = (IsAuthenticated, )

    def create(self, request, *a, **kws):
        """ Creates a Group and a Membership instance
        """
        # this serializer only deals with Group, Membership is handled below
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        # create the group
        instance = serializer.save(
            created_by=self.request.user.account,
        )
        self.fetch_unsplash(instance)

        # create member - group.created_by automatically is a member
        # while others are pending members until approved by group.created_by
        models.Membership.objects.create(
            group=instance,
            account=instance.created_by,
            approved_at=datetime.datetime.utcnow().replace(tzinfo=pytz.UTC),
        )

        Action.objects.create(
            action_object=instance,
            actor=request.user.account,
            target=None,
            timestamp=instance.created_at,
            verb=Verb.created,
        )

        return Response(
            serializers.GroupSerializer(instance).data,
            status=status.HTTP_201_CREATED
        )

    def fetch_unsplash(self, instance):
        # TODO maybe move this whole shenanigans into a task
        try:
            processing = True
            while processing:
                # get random as not to show same pic to every group
                q = f'{instance.city.ascii_name} {instance.city.country.name}'
                interest = instance.interests.all()[0].name
                q += f' {interest}'
                data = unsplash.get_random_photo(q)
                if data:
                    processing = False

            instance.unsplash = data[0]
            instance.save()

            # also look if city and country have unsplash images
            # fetch if not
            if not instance.city.images:
                q = f'{instance.city.ascii_name} {instance.city.country.name}'
                resp = unsplash.search_photo(q, per_page=5)
                instance.city.images = resp['results']
                instance.city.save()

            if not instance.city.country.images:
                q = f'{instance.city.country.name}'
                resp = unsplash.search_photo(q, per_page=5)
                instance.city.country.images = resp['results']
                instance.city.country.save()
        except Exception:
            logger.error(traceback.format_exc())


class GroupListView(generics.ListAPIView):
    """ Lists groups for request user
    """
    permission_classes = (IsAuthenticated, )
    queryset = models.Group.objects.all()
    serializer_class = serializers.GroupSerializer

    def get_queryset(self):
        expired = self.request.GET.get('expired')
        pending = self.request.GET.get('pending')

        today = datetime.datetime.utcnow().date()

        if expired is not None:
            # return only expired groups
            return models.Group.objects.filter(
                starts_at__lt=today,
                membership__account=self.request.user.account
            ).order_by('-starts_at').distinct()

        if pending is not None:
            # return only pending groups
            return models.Group.objects.filter(
                starts_at__gte=today,
                membership__account=self.request.user.account,
                membership__approved_at__isnull=True,
            ).order_by('starts_at').distinct()

        # return only upcoming trips
        return models.Group.objects.filter(
            starts_at__gte=today,
            membership__account=self.request.user.account,
            membership__approved_at__isnull=False,
        ).order_by('starts_at').distinct()


class GroupCityHotView(generics.ListAPIView):
    queryset = City.objects.all()
    serializer_class = CitySerializer
    pagination_class = None

    def get_queryset(self):
        ts = datetime.datetime.utcnow().date() + datetime.timedelta(1)
        return City.objects.annotate(
            num_groups=Count('group'),
        ).filter(
            group__starts_at__gte=ts,
            num_groups__gt=0
        ).distinct().order_by('-num_groups')[:5]


class GroupReportReasonsView(generics.GenericAPIView):
    http_method_names = ['get']
    queryset = models.GroupReport.objects.all()

    def get(self, request, **kws):
        return Response([{'id': k, 'label': str(v)}
                         for k, v in models.GroupReport.REASONS])


class GroupDetailView(generics.RetrieveAPIView, generics.UpdateAPIView):
    queryset = models.Group.objects.all()
    serializer_class = serializers.GroupSerializer
    permission_classes = (IsAuthenticated, )
    http_method_names = ['get', 'patch']

    def patch(self, request, **kws):
        obj = self.get_object()
        obj.name = request.data['name']

        if 'file' in request.data:
            obj.photo = request.data['file']

        obj.save()

        return Response({'id': obj.id, 'image': obj.image})


class GroupMembershipListView(generics.GenericAPIView):
    permission_classes = (IsAuthenticated, IsApprovedMember)

    def get(self, request, **kws):
        qs = models.Membership.objects.filter(group_id=kws['pk'])
        data = {
            m.account_id: serializers.MembershipSerializer(m).data
            for m in qs
        }
        return Response(data)


class GroupStreamView(generics.GenericAPIView):
    permission_classes = (IsAuthenticated, IsApprovedMember)
    queryset = models.Group.objects.all()
    serializer_class = serializers.ActionSerializer

    def get_stream(self):
        group = self.get_object()

        action_object_ctypes = ["proposalstay", "proposalride", "group"]

        ctypes = ContentType.objects.filter(
            model__in=action_object_ctypes
        )
        stream = any_stream(group).filter(
            verb=Verb.created,
            action_object_content_type__in=ctypes
        )
        return sorted(
            stream,
            # key=lambda a: a.action_object.updated_at,
            key=lambda a: getattr(a.action_object, 'votes_score', -1000),
            reverse=True
        )

    def get(self, request, **kws):
        stream = self.get_stream()
        page = self.paginate_queryset(stream)
        serializer = self.get_serializer(page, many=True)
        return self.get_paginated_response(serializer.data)


class GroupProposalStayOverview(generics.GenericAPIView):
    permission_classes = (IsAuthenticated, IsApprovedMember)
    queryset = models.Group.objects.all()

    def get(self, request, **kws):
        qs = self.get_object().proposalstay_set.all()
        qs = sorted(qs, key=lambda p: p.estimated_cost_eur, reverse=True)

        if len(qs) <= 1:
            return Response({
                'most_expensive': None,
                'cheapest': None
            })

        high = qs[0]
        low = qs[-1]

        most_expensive = serializers.ProposalStaySerializer(instance=high).data
        action_id = high.action_object_actions.order_by('id')[0].id
        most_expensive['action_id'] = action_id

        cheapest = serializers.ProposalStaySerializer(instance=low).data
        action_id = low.action_object_actions.order_by('id')[0].id
        cheapest['action_id'] = action_id

        return Response({
            'most_expensive': most_expensive,
            'cheapest': cheapest,
        })


class GroupProposalRideOverview(generics.GenericAPIView):
    """ Returns a list of lists.  Every list in the list contains two dicts.

    First dict is the most expensive ride and second one is the cheapest.

    Each pair represents a starting point which can be found in either dicts.
    """
    permission_classes = (IsAuthenticated, IsApprovedMember)
    queryset = models.Group.objects.all()

    def get(self, request, **kws):
        qs = self.get_object().proposalride_set.all()
        qs = sorted(qs, key=lambda p: p.estimated_cost_eur, reverse=True)

        data = defaultdict(list)

        for item in qs:
            data[item.starts_from_id].append(item)

        response = []
        for city_id, rides in data.items():
            if len(rides) <= 1:
                continue

            high = rides[0]
            low = rides[-1]
            most_expensive = serializers.ProposalRideSerializer(high).data
            action_id = high.action_object_actions.order_by('id')[0].id
            most_expensive['action_id'] = action_id

            cheapest = serializers.ProposalRideSerializer(low).data
            action_id = low.action_object_actions.order_by('id')[0].id
            cheapest['action_id'] = action_id

            response.append((most_expensive, cheapest))

        return Response(response)


class GroupSimilarsView(generics.GenericAPIView):
    queryset = models.Group.objects.all()
    permission_classes = (IsAuthenticated, )

    def get(self, request, **kws):

        max_groups = 5

        group = self.get_object()

        almost_same_start = Q(
            starts_at__gte=group.starts_at - datetime.timedelta(3)
        )
        same_city = Q(
            city_id=group.city_id
        )
        # same interests TODO
        qs = models.Group.objects.filter(
            almost_same_start, same_city
        ).exclude(id=group.id).order_by('starts_at')[:max_groups]

        # nothing found just list other groups with almost same start
        if qs.count() <= max_groups:
            remaining = max_groups - qs.count()

            exclude_ids = [group.id]
            exclude_ids.extend(g.id for g in qs)

            _qs = models.Group.objects.filter(
                almost_same_start
            ).exclude(id__in=exclude_ids).order_by('starts_at')[:remaining]

            qs = list(qs)
            qs.extend(list(_qs))

            # still nothing found just take some groups and display them
            if len(qs) == 0:
                ts = datetime.datetime.utcnow().date() + datetime.timedelta(1)
                qs = models.Group.objects.filter(
                    starts_at__gte=ts
                ).exclude(id=group.id).order_by('starts_at')[:max_groups]

        return Response([
            serializers.GroupSerializer(group).data for group in qs
        ])


class GroupReportCreateView(generics.CreateAPIView):
    queryset = models.GroupReport.objects.all()
    serializer_class = serializers.GroupReportCreateSerializer
    permission_classes = (IsAuthenticated, )


class InvitationCreateView(generics.CreateAPIView):
    permission_classes = (IsAuthenticated, CanCreateInGroup)
    queryset = models.Invitation.objects.all()
    serializer_class = serializers.InvitationCreateSerializer

    def create(self, request):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        obj = serializer.save()

        notify(
            actor=request.user.account,
            verb=Verb.created,
            action_object=obj,
            recipients=obj.group.get_notification_recipients(
                exclude=request.user.account
            )
        )
        return Response(serializer.data, status=status.HTTP_201_CREATED)


class InvitationDetailView(RefIdObject, generics.RetrieveAPIView):
    """ Free for anyone. If you have the link then this invitation is probably
    for you.

    Displays details with Accept | Reject options.
    """
    queryset = models.Invitation.objects.all()
    serializer_class = serializers.InvitationSerializer

    def get_object(self):
        obj = self.get_object_by_ref_id(model_class=models.Invitation)

        if not obj.accessed_at:
            obj.accessed_at = datetime.datetime.utcnow().replace(
                tzinfo=pytz.UTC)
            obj.save()

        return obj


class InvitationAcceptView(RefIdObject, generics.RetrieveAPIView):
    """ Turns an Invitation into a Member
    """
    permission_classes = (IsAuthenticated, )

    def post(self, request, **kws):
        invitation = self.get_object_by_ref_id(
            model_class=models.Invitation
        )

        next_route_params = {
            'id': invitation.group_id,
            'slug': invitation.group.slug,
        }

        if invitation.to_email != request.user.email:
            message = 'Looks like this invitation is not for you'
            return Response({
                'message': message,
                'route_params': next_route_params,
            }, status.HTTP_400_BAD_REQUEST)

        if invitation.accepted_at:
            message = 'You already accepted this invitiaton'
            return Response({
                'message': message,
                'route_params': next_route_params,
            }, status.HTTP_400_BAD_REQUEST)

        # create the member
        member = dict(
            group=invitation.group,
            account=self.request.user.account,
            inviter=invitation.created_by,
        )
        utcnow = datetime.datetime.utcnow().replace(tzinfo=pytz.UTC)
        if invitation.invited_by_group_admin:
            member.update({
                'approved_at': utcnow
            })
            message = 'Welcome to the group!'
            level = 'success'
        else:
            message = ('You will automatically join the group once the '
                       'admin has approved your request.')
            level = 'warning'

        invitation.accepted_at = utcnow
        invitation.accepted_by = self.request.user.account

        # might have declined earlier and changed his mind, so reset below
        invitation.declined_at = None
        invitation.declined_by = None
        invitation.save()

        logger.debug(f'Account(id={request.user.account.id}) accepted '
                     f'Invitation(id={invitation.id})')

        membership = models.Membership.objects.create(**member)

        logger.debug(f'Created Membership(id={membership.id}) base on '
                     f'Invitation(id={invitation.id})')

        recipients = invitation.group.get_notification_recipients(
            exclude=request.user.account
        )
        notify(
            actor=request.user.account,
            verb=Verb.accepted,
            action_object=invitation,
            recipients=recipients,
        )
        if invitation.invited_by_group_admin:
            notify(
                actor=request.user.account,
                verb=Verb.joined,
                action_object=invitation.group,
                recipients=recipients,
            )
        else:
            notify(
                actor=request.user.account,
                verb=Verb.requested,
                action_object=membership,
                recipients=recipients,
            )

        account_ids = invitation.group.get_approved_membership().values_list(
            'account_id', flat=True
        )
        return Response({
            'level': level,
            'message': message,
            'route_params': next_route_params,
            'account_ids': account_ids,
            'membership': serializers.MembershipSerializer(membership).data,
        })


class InvitationDeclineView(RefIdObject, generics.RetrieveAPIView):
    permission_classes = (IsAuthenticated, )

    def post(self, request, **kws):
        invitation = self.get_object_by_ref_id(
            model_class=models.Invitation
        )

        if invitation.to_email != request.user.email:
            message = 'Looks like this invitation is not for you'
            return Response({
                'message': message,
            }, status.HTTP_400_BAD_REQUEST)

        if invitation.declined_at:
            message = 'Invitation already declined'
            return Response({
                'message': message,
            }, status.HTTP_400_BAD_REQUEST)

        if invitation.accepted_at:
            message = 'Cannot decline an already accepted invitation'
            return Response({
                'message': message,
            }, status.HTTP_400_BAD_REQUEST)

        invitation.declined_at = datetime.datetime.utcnow().replace(
            tzinfo=pytz.UTC)
        invitation.declined_by = request.user.account
        invitation.save()

        message = ('Invitation was declined. In case you change your mind, '
                   'just follow again the link in your email')

        logger.debug(f'Account(id={request.user.account.id}) rejected '
                     f'Invitation(id={invitation.id})')
        notify(
            actor=request.user.account,
            verb=Verb.declined,
            action_object=invitation,
            recipients=[invitation.created_by.user]
        )
        return Response({
            'message': message,
        })


class ProposalRideChoicesView(generics.GenericAPIView):
    def get(self, request, **kws):
        return Response({
            'features': [
                {'id': k, 'label': str(v)}
                for k, v in models.ProposalRide.FEATURES
            ],
            'types': [
                {'id': k, 'label': str(v)}
                for k, v in models.ProposalRide.TYPES
            ],
            'makes': [
                {'id': k, 'label': str(v)}
                for k, v in models.ProposalRide.MAKES
            ]
        })


class ProposalRideCreateView(generics.CreateAPIView):
    permission_classes = (IsAuthenticated, CanCreateInGroup)
    queryset = models.ProposalRide.objects.all()
    serializer_class = serializers.ProposalRideCreateSerializer

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        serializer.save()

        obj = serializer.instance

        action = Action.objects.create(
            actor=request.user.account,
            verb=Verb.created,
            action_object=obj,
            target=obj.group,
        )
        notify(
            actor=request.user.account,
            verb=Verb.created,
            action_object=obj,
            recipients=obj.group.get_notification_recipients(
                exclude=request.user.account
            )
        )
        return Response(serializers.ActionSerializer(action).data,
                        status=status.HTTP_201_CREATED)


class ProposalDeleteView(generics.DestroyAPIView):
    permission_classes = (IsAuthenticated, IsObjOwner)
    queryset = None

    def delete(self, request, **kws):
        instance = self.get_object()

        group_id = instance.group_id
        content_type = instance.__class__.__name__.lower()

        instance.delete()

        return Response({
            'group_id': group_id,
            'content_type': content_type
        })


class ProposalVoteView(generics.GenericAPIView):
    http_method_names = ['patch']
    permission_classes = (IsAuthenticated, IsApprovedMember, IsValidVote)
    queryset = None

    def patch(self, request, *a, **kws):
        voted = request.data['voted']
        obj = self.get_object()

        account_id = request.user.account.id

        # remove vote first whether its an upvote or downvote
        if account_id in obj.upvotes:
            obj.upvotes.remove(account_id)

        if account_id in obj.downvotes:
            obj.downvotes.remove(account_id)

        # add upvote or downvote
        if voted is True:
            obj.upvotes.insert(0, account_id)
        elif voted is False:
            obj.downvotes.insert(0, account_id)

        obj.save()

        notify(
            actor=request.user.account,
            verb=(Verb.upvoted if voted else
                  Verb.downvoted),
            action_object=obj,
            recipients=obj.group.get_notification_recipients(
                exclude=request.user.account
            )
        )
        data = {
            'id': obj.id,
            'upvotes': obj.upvotes,
            'downvotes': obj.downvotes,
            'votes_score': obj.votes_score,
            'account_ids': obj.group.get_approved_membership().values_list(
                'account_id', flat=True
            ),
            'group_id': obj.group_id,
            'content_type': obj.__class__.__name__.lower(),
        }
        return Response(data)


class ProposalStayCreateView(generics.CreateAPIView):
    permission_classes = (IsAuthenticated, CanCreateInGroup)
    queryset = models.ProposalStay.objects.all()
    serializer_class = serializers.ProposalStayCreateSerializer

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        # if user posted the proposal too quick, then url_data is an empty dict
        # try to parse again the url and fetch proper url_data
        if not serializer.validated_data.get('url_data'):
            try:
                serializer.validated_data['url_data'] = parse_head(
                    serializer.validated_data['url']
                )
            except ParseHeadInvalidUrl:
                return Response({
                    'url': ["This doesn't look like a valid accommodation "
                            "URL."]
                }, status=status.HTTP_400_BAD_REQUEST)

        serializer.save()

        obj = serializer.instance

        action = Action.objects.create(
            action_object=obj,
            actor=request.user.account,
            target=obj.group,
            timestamp=obj.created_at,
            verb=Verb.created,
        )
        notify(
            actor=request.user.account,
            verb=Verb.created,
            action_object=obj,
            recipients=obj.group.get_notification_recipients(
                exclude=request.user.account
            )
        )
        return Response(serializers.ActionSerializer(action).data,
                        status=status.HTTP_201_CREATED)


# NOTE regarding notifications relative to group memberships
# because memberships might get deleted (not approved, kicked out, etc)
# we wont use membership instances as action objects, not to end up with
# notifications with null action objects
# hence we use the membership account as action object
# and membership group as target


class MembershipCreateView(generics.CreateAPIView):
    permission_classes = (IsAuthenticated, )
    queryset = models.Membership.objects.all()
    serializer_class = serializers.MembershipCreateSerializer

    def create(self, request):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        membership = serializer.save()

        notify(
            actor=request.user.account,
            verb=Verb.requested,
            action_object=membership.account,
            target=membership.group,
            recipients=[membership.group.created_by.user]
        )
        data = serializers.MembershipSerializer(membership).data
        return Response(data, status=status.HTTP_201_CREATED)


class MembershipApproveView(generics.UpdateAPIView):
    permission_classes = (IsAuthenticated, IsGroupAdmin)
    queryset = models.Membership.objects.all()
    serializer_class = serializers.MembershipSerializer

    def patch(self, request, **kws):
        membership = self.get_object()
        membership.approved_at = datetime.datetime.utcnow().replace(
            tzinfo=pytz.UTC
        )
        membership.save()

        notify(
            actor=request.user.account,
            verb=Verb.approved,
            action_object=membership.account,
            target=membership.group,
            recipients=[membership.account.user]
        )
        return Response(serializers.MembershipSerializer(membership).data)


class MembershipDeclineView(generics.DestroyAPIView):
    """ Deletes the membership instance
    """
    permission_classes = (IsAuthenticated, IsGroupAdmin)
    queryset = models.Membership.objects.all()

    def delete(self, request, **kws):
        membership = self.get_object()

        if membership.approved_at is None:
            notify(
                actor=request.user.account,
                verb=Verb.declined,
                action_object=membership.account,
                target=membership.group,
                recipients=[membership.account.user],
            )

            account_id = membership.account_id
            group_id = membership.group_id
            membership.delete()

            return Response({
                'account_id': account_id,
                'group_id': group_id,
            })

        return Response({
            'message': 'Cannot decline an already accepted membership'
        }, status=status.HTTP_400_BAD_REQUEST)


class ActionDetailView(generics.RetrieveAPIView):
    queryset = Action.objects.all()
    serializer_class = serializers.ActionSerializer
    permission_classes = (IsAuthenticated, IsApprovedMember)
    http_method_names = ['get']


class GroupStartsAtFilterBackend(object):
    def filter_queryset(self, request, queryset, view):
        try:
            starts_at__gte = request.GET.get('starts_at__gte')
            ts = datetime.datetime.strptime(starts_at__gte, '%Y-%m-%d').date()
        except (TypeError, ValueError):
            # starting from tomorrow if no custom query string was sent
            ts = datetime.datetime.utcnow().date() + datetime.timedelta(1)

        # return queryset.filter(starts_at__gte=ts).order_by('starts_at')
        return queryset.filter(starts_at__gte=ts)


class ExploreGroupListView(generics.ListAPIView):
    queryset = models.Group.objects.all()
    serializer_class = serializers.GroupSerializer
    filter_backends = [GroupStartsAtFilterBackend]


class ExploreInterestListView(generics.ListAPIView):
    queryset = models.Interest.objects.filter(is_active=True)
    serializer_class = serializers.InterestSerializer

    def get_queryset(self):
        ts = datetime.datetime.utcnow().date() + datetime.timedelta(1)

        return models.Interest.objects.filter(is_active=True).annotate(
            groups_count=Count('group')
        ).filter(
            group__starts_at__gte=ts,
        ).order_by('-groups_count')


class ExploreInterestGroupListView(generics.ListAPIView):
    queryset = models.Group.objects.all()
    serializer_class = serializers.GroupSerializer
    filter_backends = [GroupStartsAtFilterBackend]

    def get_queryset(self):
        interest = models.Interest.objects.get(slug=self.kwargs['slug'])
        return interest.group_set.all()


class ExploreCountryListView(generics.ListAPIView):
    queryset = Country.objects.all()
    serializer_class = CountrySerializer

    def get_queryset(self):
        ts = datetime.datetime.utcnow().date() + datetime.timedelta(1)

        return Country.objects.filter(
            city__group__starts_at__gte=ts,
        ).distinct().order_by(
            'continent__name', 'name',
        )


class ExploreCountryCityListView(generics.ListAPIView):
    queryset = City.objects.all()
    serializer_class = CitySerializer

    def get_queryset(self):
        country = Country.objects.get(slug=self.kwargs['slug'])
        ts = datetime.datetime.utcnow().date() + datetime.timedelta(1)

        return country.city_set.filter(
            group__starts_at__gte=ts,
        ).distinct().order_by('name')


class ExploreCityGroupListView(generics.ListAPIView):
    serializer_class = serializers.GroupSerializer
    filter_backends = [GroupStartsAtFilterBackend]

    def get_queryset(self):
        city = City.objects.get(id=self.kwargs['pk'])
        return city.group_set.all()


# Admin only unsplash images dashboard
class InterestListAllView(generics.ListAPIView):
    permission_classes = (IsSuperUser, )
    queryset = models.Interest.objects.all()
    serializer_class = serializers.InterestFullSerializer


class InterestUpdateView(generics.UpdateAPIView):
    permission_classes = (IsSuperUser, )
    queryset = models.Interest.objects.all()
    serializer_class = serializers.InterestFullSerializer


class CountryListView(generics.ListAPIView):
    permission_classes = (IsSuperUser, )
    serializer_class = CountryFullSerializer
    queryset = Country.objects.all()

    def get_queryset(self):
        ts = datetime.datetime.utcnow().date() + datetime.timedelta(1)

        return Country.objects.filter(
            city__group__starts_at__gte=ts,
        ).distinct().order_by('images', 'continent__name', 'name')


class CountryUpdateView(generics.UpdateAPIView):
    permission_classes = (IsSuperUser, )
    queryset = Country.objects.all()
    serializer_class = CountryFullSerializer


class CityListView(generics.ListAPIView):
    permission_classes = (IsSuperUser, )
    serializer_class = CityFullSerializer

    queryset = City.objects.all()

    def get_queryset(self):
        ts = datetime.datetime.utcnow().date() + datetime.timedelta(1)

        return City.objects.filter(
            group__starts_at__gte=ts,
        ).distinct().order_by('images', 'name')


class CityUpdateView(generics.UpdateAPIView):
    permission_classes = (IsSuperUser, )
    queryset = City.objects.all()
    serializer_class = CityFullSerializer
