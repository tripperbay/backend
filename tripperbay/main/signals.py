from asgiref.sync import async_to_sync

from django.conf import settings
from django.contrib.auth.models import User
from django.db.models import signals
from django.dispatch import receiver
from django.utils.text import slugify

from allauth.socialaccount.models import SocialAccount

from notifications.models import Notification

from tripperbay.chat.models import Room
from tripperbay.consumers import websocket_broadcast
from tripperbay.main import models, utils
from tripperbay.main.logger import logger
from tripperbay.main.serializers import NotificationSerializer
from tripperbay.main.utils import hashid_compute


# XXX naming scheme for receivers: verb_entity[_attribute]


@receiver(signals.pre_save, sender=User)
def set_user_first_name(sender, instance, **kws):
    """ Sets username as first name if first name does not exist
    This happens when users register directly with a tripperbay account
    """
    if instance.first_name:
        return
    instance.first_name = instance.username.title()


@receiver(signals.post_save, sender=User)
def create_account(sender, instance, created, **kws):
    try:
        instance.account
    except models.Account.DoesNotExist:
        account = models.Account(user=instance)
        account.save()
        logger.debug(f'Created Account(id={account.id}) for new '
                     f'User(id={instance.id})')


@receiver(signals.post_save, sender=SocialAccount)
def refresh_account_image(sender, instance, created, **kws):
    """ Fetch facebook profile picture and store it on Account
    """
    if instance.provider != 'facebook':
        return

    relative_filename = models.Account.get_facebook_image(instance.uid)
    instance.user.account.image = relative_filename
    instance.user.account.save()


@receiver(signals.post_save, sender=Room)
@receiver(signals.post_save, sender=models.Invitation)
def compute_ref_id(sender, instance, created, **kws):
    """ Computes ref_id value with salt being the class name of the instance

    Correlated with RefIdTimestampModel.ref_id_unveiled
    """
    if not instance.ref_id:
        instance.ref_id = hashid_compute(
            instance.id, salt=instance.__class__.__name__
        )
        # use bulk update not to trigger again post_save signal
        sender.objects.bulk_update([instance], ['ref_id'])


@receiver(signals.post_save, sender=models.Invitation)
def send_invitation_email(sender, instance, created, **kws):
    if not created:
        return

    utils.send_email('main/email/invitation_sent', {
        'site_name': settings.SITE_NAME,
        'site_domain': settings.SITE_DOMAIN,
        'site_url': settings.SITE_URL,
        'destination': instance.group.city.name,
        'full_name': instance.created_by.user.get_full_name(),
        'first_name': instance.created_by.user.first_name,
        'leave_date': instance.group.starts_at.strftime('%A, %d %b'),
        'invitation_url': f'/invitation/{instance.ref_id}'
    }, [instance.to_email])

    logger.debug(f'Sent email for Invitation(id={instance.id})')


@receiver(signals.post_save, sender=Notification)
def broadcast_notification_websocket(sender, instance, created, **kws):
    if not created:  # dont do anything
        return

    # broadcast notification to recipient
    async_to_sync(websocket_broadcast)(
        NotificationSerializer(instance).data,
        account_ids=[instance.recipient.account.id],
        action='notificationCreate',
    )


@receiver(signals.pre_save, sender=models.SavedSearch)
@receiver(signals.pre_save, sender=models.Group)
def set_city_slug(sender, instance, **kws):
    instance.slug = slugify(
        f'{instance.city.ascii_name} {instance.city.country.name}'
    )


@receiver(signals.pre_save, sender=models.Interest)
def set_interest_slug(sender, instance, **kws):
    instance.slug = slugify(instance.name)
