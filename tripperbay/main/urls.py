from django.conf.urls import url
from tripperbay.main import views

app_name = 'main'

urlpatterns = [
    url(r'^account/$',
        views.AccountDetailView.as_view(), name='account_detail'),
    url(r'^account/notification/list/$',
        views.AccountNotificationListView.as_view(),
        name='account_notification_list'),
    url(r'^account/notifications/count/unread/$',
        views.AccountNotificationsCountUnreadView.as_view(),
        name='account_notifications_count_unread'),
    url(r'^account/notifications/mark-as-read/$',
        views.AccountNotificationsMarkAsReadView.as_view(),
        name='account_notifications_mark_as_read'),

    url(r'^interest/list/$',
        views.InterestListView.as_view(), name='interest_list'),

    url(r'^currency/list/$',
        views.CurrencyListView.as_view(), name='currency_list'),

    url(r'^search/create/$',
        views.SavedSearchCreateView.as_view(), name='saved_search_create'),
    url(r'^search/(?P<slug>[\w-]+)/$',
        views.SavedSearchDetailView.as_view(), name='saved_search_detail'),

    url(r'^group/create/$',
        views.GroupCreateView.as_view(), name='group_create'),
    url(r'^group/report/reasons/$',
        views.GroupReportReasonsView.as_view(), name='group_report_reasons'),
    url(r'^group/list/$',
        views.GroupListView.as_view(), name='group_list'),

    url(r'^group/city/hot/$',
        views.GroupCityHotView.as_view(), name='group_city_hot'),

    url(r'^group/(?P<pk>\d+)/$',
        views.GroupDetailView.as_view(), name='group_detail'),
    url(r'^group/(?P<pk>\d+)/membership/list/$',
        views.GroupMembershipListView.as_view(), name='group_membership_list'),
    url(r'^group/(?P<pk>\d+)/stream/$',
        views.GroupStreamView.as_view(), name='group_stream'),
    url(r'^group/(?P<pk>\d+)/proposal/stay/overview/$',
        views.GroupProposalStayOverview.as_view(), name='group_proposal_stay_overview'),  # noqa
    url(r'^group/(?P<pk>\d+)/proposal/ride/overview/$',
        views.GroupProposalRideOverview.as_view(), name='group_proposal_ride_overview'),  # noqa
    url(r'^group/(?P<pk>\d+)/similars/$',
        views.GroupSimilarsView.as_view(), name='group_similars'),
    url(r'^group/(?P<pk>\d+)/report/$',
        views.GroupReportCreateView.as_view(), name='group_report'),

    url(r'^invitation/create/$',
        views.InvitationCreateView.as_view(), name='invitation_create'),
    url(r'^invitation/(?P<ref_id>\w+)/$',
        views.InvitationDetailView.as_view(), name='invitation_detail'),
    url(r'^invitation/(?P<ref_id>\w+)/accept/$',
        views.InvitationAcceptView.as_view(), name='invitation_accept'),
    url(r'^invitation/(?P<ref_id>\w+)/decline/$',
        views.InvitationDeclineView.as_view(), name='invitation_decline'),

    url(r'^proposal/ride/choices/$',
        views.ProposalRideChoicesView.as_view(), name='proposal_ride_choices'),
    url(r'^proposal/ride/create/$',
        views.ProposalRideCreateView.as_view(), name='proposal_ride_create'),
    url(r'^proposal/ride/vote/(?P<pk>\d+)/$',
        views.ProposalVoteView.as_view(
            queryset=views.models.ProposalRide.objects.all()
        ), name='proposal_ride_vote'),
    url(r'^proposal/ride/delete/(?P<pk>\d+)/$',
        views.ProposalDeleteView.as_view(
            queryset=views.models.ProposalRide.objects.all()
        ), name='proposal_ride_delete'),

    url(r'^proposal/stay/create/$',
        views.ProposalStayCreateView.as_view(), name='proposal_stay_create'),
    url(r'^proposal/stay/vote/(?P<pk>\d+)/$',
        views.ProposalVoteView.as_view(
            queryset=views.models.ProposalStay.objects.all()
        ), name='proposal_stay_vote'),
    url(r'^proposal/stay/delete/(?P<pk>\d+)/$',
        views.ProposalDeleteView.as_view(
            queryset=views.models.ProposalStay.objects.all()
        ), name='proposal_stay_delete'),

    url(r'^membership/create/$',
        views.MembershipCreateView.as_view(), name='membership_create'),
    url(r'^membership/(?P<pk>\d+)/approve/$',
        views.MembershipApproveView.as_view(), name='membership_approve'),
    url(r'^membership/(?P<pk>\d+)/decline/$',
        views.MembershipDeclineView.as_view(), name='membership_decline'),

    url(r'^action/(?P<pk>\d+)/$',
        views.ActionDetailView.as_view(), name='action_detail'),

    url(r'^explore/group/list/$',
        views.ExploreGroupListView.as_view(), name='explore_group_list'),
    url(r'^explore/interest/list/$',
        views.ExploreInterestListView.as_view(), name='explore_interest_list'),
    url(r'^explore/interest/(?P<slug>[\w-]+)/group/list/$',
        views.ExploreInterestGroupListView.as_view(), name='explore_interest_group_list'),  # noqa

    url(r'^explore/country/list/$',
        views.ExploreCountryListView.as_view(), name='explore_country_list'),
    url(r'^explore/country/(?P<slug>[\w-]+)/city/list/$',
        views.ExploreCountryCityListView.as_view(), name='explore_country_city_list'),  # noqa
    url(r'^explore/city/(?P<pk>\d+)/group/list/$',
        views.ExploreCityGroupListView.as_view(), name='explore_city_group_list'),  # noqa

    # Admin only - unsplash images dashboard
    url(r'^unsplash/interest/list/$',
        views.InterestListAllView.as_view(), name='interest_list_all'),
    url(r'^unsplash/interest/(?P<pk>\d+)/update/$',
        views.InterestUpdateView.as_view(), name='interest_update'),
    url(r'^unsplash/country/list/$',
        views.CountryListView.as_view(), name='country_list'),
    url(r'^unsplash/country/(?P<pk>\w+)/update/$',
        views.CountryUpdateView.as_view(), name='country_update'),
    url(r'^unsplash/city/list/$',
        views.CityListView.as_view(), name='city_list'),
    url(r'^unsplash/city/(?P<pk>\d+)/update/$',
        views.CityUpdateView.as_view(), name='city_update'),
]
