import os
import requests

from PIL import Image
from io import BytesIO

from django.conf import settings
from django.contrib.auth.models import User
from django.contrib.postgres.fields import ArrayField, JSONField
from django.db import models
from django.urls import reverse
from django.utils.translation import gettext_lazy as _

from tripperbay.geo.models import City
from tripperbay.main.logger import logger
from tripperbay.main.utils import hashid_unveil, currency_convert
from tripperbay.unsplash.models import UnsplashAbstractModel


class TimestampModel(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class RefIdTimestampModel(TimestampModel):
    """ Abstract model with ref_id field where hashid values are stored to

    The ref_id value is being computed on post_save signals, so every model
    which inherits from this one should be linked with a compute_ref_id
    receiver
    """
    ref_id = models.CharField(max_length=32, default='', blank=True)

    class Meta:
        abstract = True

    @property
    def ref_id_unveiled(self):
        return hashid_unveil(self.ref_id, salt=self.__class__.__name__)


class AccountManager(models.Manager):
    def get_queryset(self):
        qs = super(AccountManager, self).get_queryset()
        return qs.select_related('user').prefetch_related('membership_set')


class Account(models.Model):
    objects = AccountManager()

    user = models.OneToOneField(User, on_delete=models.CASCADE)

    image = models.ImageField(blank=True, upload_to='account')

    @property
    def image_src(self):
        if self.image:
            return self.image.url
        return '/static/img/dummy_account.png'

    @staticmethod
    def get_facebook_image(fb_id):
        """ Fetches facebook profile picture for the given fb_id, stores it
        on disk and returns the relative filename according to
        Account.image.upload_to
        """
        filename = f'{Account.image.field.upload_to}/fb_{fb_id}.jpg'
        pathname = os.path.join(settings.MEDIA_ROOT, filename)

        if os.path.isfile(pathname):
            return filename

        try:
            resp = requests.get(
                'https://graph.facebook.com/v2.8/{fb_id}/picture'.format(
                    fb_id=fb_id))

            image = Image.open(BytesIO(resp.content))
            image.save(pathname, format="JPEG")

            logger.debug(f'Fetched facebook picture for fb id {fb_id}')

        except Exception as exc:
            logger.error(f'Failed to fetch facebook picture {exc}')
            return None

        return filename

    def get_approved_membership(self):
        return self.membership_set.filter(approved_at__isnull=False)


dirname = os.path.join(settings.MEDIA_ROOT, Account.image.field.upload_to)
if not os.path.isdir(dirname):
    os.mkdir(dirname)


class Currency(models.Model):
    id = models.CharField(max_length=4, primary_key=True)
    name = models.CharField(max_length=32)


class Interest(UnsplashAbstractModel):
    name = models.CharField(max_length=32)
    # slug below computed in pre save signal
    slug = models.CharField(max_length=32)
    descr = models.CharField(max_length=512, default='', blank=True)

    is_active = models.BooleanField(default=False)

    class Meta:
        ordering = ('name', )

    def __str__(self):
        return f'{self.name}'


class GroupManager(models.Manager):
    def get_queryset(self):
        qs = super(GroupManager, self)
        return qs.get_queryset().select_related(
            'city',
            'created_by',
            'created_by__user',
        ).prefetch_related(
            'interests'
        )


def group_photo_upload_to(instance, filename):
    return f'group/{instance.id}/{filename}'


class Group(TimestampModel):
    objects = GroupManager()

    # a default name is set in post save signal
    name = models.CharField(max_length=256, blank=True, default='')

    created_by = models.ForeignKey(
        Account, models.CASCADE, related_name='owned_groups'
    )
    starts_at = models.DateField()
    ends_at = models.DateField()

    city = models.ForeignKey(City, models.CASCADE)
    # city slug below computed in pre save signal
    slug = models.CharField(max_length=32)

    interests = models.ManyToManyField(Interest, blank=True)

    members = models.ManyToManyField(Account,
                                     through='Membership',
                                     through_fields=('group', 'account'))

    unsplash = JSONField(blank=True, default=dict)

    photo = models.ImageField(blank=True, upload_to=group_photo_upload_to)

    def __str__(self):
        return f'{self.city.name}: {self.starts_at} {self.ends_at}'

    def get_approved_membership(self):
        return self.membership_set.filter(approved_at__isnull=False)

    def get_notification_recipients(self, exclude=None):
        """ Returns a list of User instances
        Each User has an account which is an approved member in Group (self)

        :param exclude: Account instance to exclude from the membership set
            (usually request.user.account, ie one who triggers the notif)
        """
        approved = self.get_approved_membership()
        if exclude is not None:
            assert isinstance(exclude, Account)
            approved = approved.exclude(account_id=exclude.id)

        return [m.account.user for m in approved]

    @property
    def duration_nights(self):
        return (self.ends_at - self.starts_at).days

    @property
    def duration_days(self):
        return self.duration_nights + 1

    @property
    def image(self):
        if self.photo:
            return {
                'dummy': True,
                'urls': {
                    'full': self.photo.url,
                    'regular': self.photo.url,
                    'small': self.photo.url,
                    'thumb': self.photo.url,
                },
            }

        if self.unsplash:
            return self.unsplash

        return {
            'dummy': True,
            'urls': {
                'full': '/static/img/dummy.png',
                'regular': '/static/img/dummy.png',
                'small': '/static/img/dummy.png',
                'thumb': '/static/img/dummy.png',
            },
        }


class GroupReport(TimestampModel):
    group = models.ForeignKey(Group, models.CASCADE)
    account = models.ForeignKey(Account, models.CASCADE)

    EXPLICIT_CONTENT = 'explicit-content'
    MISLEADING = 'misleading'
    OFFENSIVE = 'offensive'
    OTHER = 'other'
    SCAM = 'scam'
    SPAM = 'spam'
    VIOLENT = 'violent'

    REASONS = (
        (EXPLICIT_CONTENT, _('Explicit Content')),
        (MISLEADING, _('Misleading')),
        (OFFENSIVE, _('Offensive')),
        (SCAM, _('Scam')),
        (SPAM, _('Spam')),
        (VIOLENT, _('Violent')),
        (OTHER, _('Other')),
    )
    reasons = ArrayField(models.CharField(max_length=32, choices=REASONS))
    reasons_other = models.CharField(max_length=256, blank=True, default='')


class Invitation(RefIdTimestampModel):
    """ Invitation sent by a member to some friend who does not have an account
    yet.

    Once the account is created, this will be deleted and a Member row
    will get created.
    """
    group = models.ForeignKey(Group, models.CASCADE)
    created_by = models.ForeignKey(Account, models.CASCADE)

    accepted_by = models.ForeignKey(Account, models.CASCADE,
                                    null=True, blank=True,
                                    related_name='invitation_set_accepted')

    declined_by = models.ForeignKey(Account, models.CASCADE,
                                    null=True, blank=True,
                                    related_name='invitation_set_declined')

    to_email = models.EmailField()

    accessed_at = models.DateTimeField(null=True, blank=True)
    accepted_at = models.DateTimeField(null=True, blank=True)
    declined_at = models.DateTimeField(null=True, blank=True)

    @property
    def invited_by_group_admin(self):
        return self.created_by == self.group.created_by


class MembershipManager(models.Manager):
    def get_queryset(self):
        qs = super(MembershipManager, self)
        return qs.get_queryset().select_related(
            'account',
            'account__user',
            'group',
            'group__created_by',
            'inviter',
            'inviter__user',
        )


class Membership(TimestampModel):
    objects = MembershipManager()

    group = models.ForeignKey(Group, models.CASCADE)
    account = models.ForeignKey(Account, models.CASCADE)

    # NOTE group admins and accounts who request to join have no inviter
    inviter = models.ForeignKey(Account, models.CASCADE,
                                blank=True, null=True,
                                related_name='membership_invites')

    approved_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        ordering = ['approved_at']
        unique_together = ('group_id', 'account_id')

    @property
    def url_approve(self):
        return reverse('api_main:membership_approve', args=(self.id,))

    @property
    def url_decline(self):
        return reverse('api_main:membership_decline', args=(self.id,))


class ProposalStayManager(models.Manager):
    def get_queryset(self):
        qs = super(ProposalStayManager, self)
        return qs.get_queryset().select_related(
            'currency',
            'group',
            'group__created_by',
            'created_by',
            'created_by__user',
        )


class ProposalStay(TimestampModel):
    objects = ProposalStayManager()

    group = models.ForeignKey(Group, models.CASCADE)
    created_by = models.ForeignKey(Account, models.CASCADE)

    upvotes = ArrayField(models.IntegerField(), blank=True, default=list)
    downvotes = ArrayField(models.IntegerField(), blank=True, default=list)

    price = models.PositiveIntegerField()  # per night
    currency = models.ForeignKey(Currency, models.CASCADE)

    available_spots = models.PositiveIntegerField()

    url = models.URLField(max_length=1024)
    url_data = JSONField(blank=True, default=dict)

    @property
    def url_vote(self):
        return reverse('api_main:proposal_stay_vote', args=(self.id,))

    @property
    def url_delete(self):
        return reverse('api_main:proposal_stay_delete', args=(self.id,))

    @property
    def votes_score(self):
        return len(self.upvotes) - len(self.downvotes)

    @property
    def estimated_cost(self):
        total = self.group.duration_nights * self.price
        return round(total / self.available_spots, 2)

    @property
    def estimated_cost_eur(self):
        return currency_convert(self.estimated_cost, self.currency_id)


class ProposalRideManager(models.Manager):
    def get_queryset(self):
        qs = super(ProposalRideManager, self)
        return qs.get_queryset().select_related(
            'created_by',
            'created_by__user',
            'currency',
            # 'group',
            # 'group__created_by',
            'starts_from',
            'starts_from__country',
            'starts_from__country_division',
        )


class ProposalRide(TimestampModel):
    objects = ProposalRideManager()

    group = models.ForeignKey(Group, models.CASCADE)
    created_by = models.ForeignKey(Account, models.CASCADE)

    upvotes = ArrayField(models.IntegerField(), blank=True, default=list)
    downvotes = ArrayField(models.IntegerField(), blank=True, default=list)

    starts_from = models.ForeignKey(City, models.CASCADE)

    # gas price per traveled distance self.starts_from to self.group.city
    price = models.PositiveIntegerField()
    currency = models.ForeignKey(Currency, models.CASCADE)

    available_seats = models.PositiveIntegerField()

    AIRBAGS = 'airbags'
    BICYCLE_RACK = 'bicycle-rack'
    HEATED_SEATS = 'heated-seats'
    ROOFTOP_CARGO_BOX = 'rooftop-cargo-box'
    SAFETY_KIT = 'safety-kit'
    SUNROOF = 'sunroof'
    USB_CHARGING = 'usb-charging'

    FEATURES = (
        (AIRBAGS, _('Airbags')),
        (BICYCLE_RACK, _('Bicycle Rack')),
        (HEATED_SEATS, _('Heated Seats')),
        (ROOFTOP_CARGO_BOX, _('Rooftop Cargo Box')),
        (SAFETY_KIT, _('Safety Kit')),
        (SUNROOF, _('Sunroof')),
        (USB_CHARGING, _('USB Charging')),
    )
    features = ArrayField(
        models.CharField(choices=FEATURES, max_length=32),
        blank=True, default=list
    )

    CABRIO = 'cabrio'
    COUPE = 'coupe'
    HATCHBACK = 'hatchback'
    MINIBUS = 'minibus'
    PICKUP_TRUCK = 'pickup-truck'
    SEDAN = 'sedan'
    SMALL_CAR = 'small-car'
    STATION_WAGON = 'station-wagon'
    SUV = 'suv'
    VAN = 'van'

    TYPES = (
        (CABRIO, _('Cabriolet')),
        (COUPE, _('Coupe')),
        (HATCHBACK, _('Hatchback')),
        (MINIBUS, _('Minibus')),
        (PICKUP_TRUCK, _('Pickup Truck')),
        (SEDAN, _('Sedan')),
        (SMALL_CAR, _('Small Car')),
        (STATION_WAGON, _('Station Wagon')),
        (SUV, _('SUV')),
        (VAN, _('Van')),
    )
    type = models.CharField(choices=TYPES, max_length=24)

    MAKES = (
        ('AC', 'AC'),
        ('ALPINA', 'ALPINA'),
        ('Abarth', 'Abarth'),
        ('Acura', 'Acura'),
        ('Aixam', 'Aixam'),
        ('Alfa Romeo', 'Alfa Romeo'),
        ('Artega', 'Artega'),
        ('Asia Motors', 'Asia Motors'),
        ('Aston Martin', 'Aston Martin'),
        ('Audi', 'Audi'),
        ('Austin', 'Austin'),
        ('Austin Healey', 'Austin Healey'),
        ('BMW', 'BMW'),
        ('Bentley', 'Bentley'),
        ('Borgward', 'Borgward'),
        ('Brilliance', 'Brilliance'),
        ('Bugatti', 'Bugatti'),
        ('Buick', 'Buick'),
        ('Cadillac', 'Cadillac'),
        ('Casalini', 'Casalini'),
        ('Caterham', 'Caterham'),
        ('Chatenet', 'Chatenet'),
        ('Chevrolet', 'Chevrolet'),
        ('Chrysler', 'Chrysler'),
        ('Citroën', 'Citroën'),
        ('Cobra', 'Cobra'),
        ('Corvette', 'Corvette'),
        ('Cupra', 'Cupra'),
        ('DS Automobiles', 'DS Automobiles'),
        ('Dacia', 'Dacia'),
        ('Daewoo', 'Daewoo'),
        ('Daihatsu', 'Daihatsu'),
        ('DeTomaso', 'DeTomaso'),
        ('Dodge', 'Dodge'),
        ('Donkervoort', 'Donkervoort'),
        ('Ferrari', 'Ferrari'),
        ('Fiat', 'Fiat'),
        ('Fisker', 'Fisker'),
        ('Ford', 'Ford'),
        ('GAC Gonow', 'GAC Gonow'),
        ('GMC', 'GMC'),
        ('Gemballa', 'Gemballa'),
        ('Grecav', 'Grecav'),
        ('Hamann', 'Hamann'),
        ('Holden', 'Holden'),
        ('Honda', 'Honda'),
        ('Hummer', 'Hummer'),
        ('Hyundai', 'Hyundai'),
        ('Infiniti', 'Infiniti'),
        ('Isuzu', 'Isuzu'),
        ('Iveco', 'Iveco'),
        ('Jaguar', 'Jaguar'),
        ('Jeep', 'Jeep'),
        ('KTM', 'KTM'),
        ('Kia', 'Kia'),
        ('Koenigsegg', 'Koenigsegg'),
        ('Lada', 'Lada'),
        ('Lamborghini', 'Lamborghini'),
        ('Lancia', 'Lancia'),
        ('Land Rover', 'Land Rover'),
        ('Landwind', 'Landwind'),
        ('Lexus', 'Lexus'),
        ('Ligier', 'Ligier'),
        ('Lincoln', 'Lincoln'),
        ('Lotus', 'Lotus'),
        ('MG', 'MG'),
        ('MINI', 'MINI'),
        ('Mahindra', 'Mahindra'),
        ('Maserati', 'Maserati'),
        ('Maybach', 'Maybach'),
        ('Mazda', 'Mazda'),
        ('McLaren', 'McLaren'),
        ('Mercedes-Benz', 'Mercedes-Benz'),
        ('Microcar', 'Microcar'),
        ('Mitsubishi', 'Mitsubishi'),
        ('Morgan', 'Morgan'),
        ('NSU', 'NSU'),
        ('Nissan', 'Nissan'),
        ('Oldsmobile', 'Oldsmobile'),
        ('Opel', 'Opel'),
        ('Pagani', 'Pagani'),
        ('Peugeot', 'Peugeot'),
        ('Piaggio', 'Piaggio'),
        ('Plymouth', 'Plymouth'),
        ('Polestar', 'Polestar'),
        ('Pontiac', 'Pontiac'),
        ('Porsche', 'Porsche'),
        ('Proton', 'Proton'),
        ('Renault', 'Renault'),
        ('Rolls-Royce', 'Rolls-Royce'),
        ('Rover', 'Rover'),
        ('Ruf', 'Ruf'),
        ('Saab', 'Saab'),
        ('Santana', 'Santana'),
        ('Seat', 'Seat'),
        ('Skoda', 'Skoda'),
        ('Smart', 'Smart'),
        ('Spyker', 'Spyker'),
        ('Ssangyong', 'Ssangyong'),
        ('Subaru', 'Subaru'),
        ('Suzuki', 'Suzuki'),
        ('TECHART', 'TECHART'),
        ('TVR', 'TVR'),
        ('Talbot', 'Talbot'),
        ('Tata', 'Tata'),
        ('Tesla', 'Tesla'),
        ('Toyota', 'Toyota'),
        ('Trabant', 'Trabant'),
        ('Triumph', 'Triumph'),
        ('Volkswagen', 'Volkswagen'),
        ('Volvo', 'Volvo'),
        ('Wartburg', 'Wartburg'),
        ('Westfield', 'Westfield'),
        ('Wiesmann', 'Wiesmann'),
    )
    make = models.CharField(choices=MAKES, max_length=16)

    @property
    def url_vote(self):
        return reverse('api_main:proposal_ride_vote', args=(self.id,))

    @property
    def url_delete(self):
        return reverse('api_main:proposal_ride_delete', args=(self.id,))

    @property
    def votes_score(self):
        return len(self.upvotes) - len(self.downvotes)

    @property
    def estimated_cost(self):
        # +1 because we add the driver
        return round(self.price / (self.available_seats + 1), 2)

    @property
    def estimated_cost_eur(self):
        return currency_convert(self.estimated_cost, self.currency_id)


class SavedSearchManager(models.Manager):
    def get_queryset(self):
        qs = super(SavedSearchManager, self)
        return qs.get_queryset().select_related('city')


class SavedSearch(TimestampModel):
    objects = SavedSearchManager()

    city = models.ForeignKey(City, models.CASCADE)
    # city slug below computed in pre save signal
    slug = models.CharField(max_length=32)
    remote_addr = models.CharField(max_length=16)
    http_user_agent = models.CharField(max_length=512)

    account = models.ForeignKey(Account, models.CASCADE, blank=True, null=True)
