from django.contrib.auth.models import User
from django.contrib.contenttypes.models import ContentType
from django.utils.translation import gettext as _

from actstream.models import Action
from notifications.models import Notification
from rest_framework import serializers

from tripperbay.main import models
from tripperbay.geo.serializers import CitySerializer


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = (
            'date_joined',
            'email',
            'first_name',
            'get_full_name',
            'id',
            'is_superuser',
            'last_login',
            'username',
        )


class AccountSerializer(serializers.ModelSerializer):
    user = UserSerializer()

    class Meta:
        model = models.Account
        fields = (
            'id',
            'image_src',
            'user',
        )


class InterestSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Interest
        fields = ('id', 'name', 'image', 'slug')


class InterestFullSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Interest
        fields = '__all__'


class CurrencySerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Currency
        fields = '__all__'


class SavedSearchCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.SavedSearch
        fields = (
            'city',
        )


class SavedSearchSerializer(serializers.ModelSerializer):
    city = CitySerializer()

    class Meta:
        model = models.SavedSearch
        fields = (
            'city',
            'id',
            'slug',
        )


class GroupCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Group
        fields = (
            'city',
            'ends_at',
            'interests',
            'name',
            'starts_at',
        )


class MembershipSerializerV2(serializers.ModelSerializer):
    account = AccountSerializer()

    class Meta:
        model = models.Membership
        fields = (
            'id',
            'account',
            'approved_at',
        )


class GroupSerializer(serializers.ModelSerializer):
    city = CitySerializer()
    created_by = AccountSerializer()
    interests = InterestSerializer(many=True)
    membership_set = MembershipSerializerV2(many=True)

    class Meta:
        model = models.Group
        fields = (
            'city',
            'created_at',
            'created_by',
            'created_by_id',
            'duration_nights',
            'ends_at',
            'id',
            'image',
            'interests',
            'membership_set',
            'name',
            'slug',
            'starts_at',
        )


class GroupReportCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.GroupReport
        fields = (
            'group',
            'account',
            'reasons',
            'reasons_other',
        )


class InvitationSerializer(serializers.ModelSerializer):
    created_by = AccountSerializer()
    group = GroupSerializer()

    class Meta:
        model = models.Invitation
        fields = (
            'accepted_at',
            'accepted_by_id',
            'accessed_at',
            'created_at',
            'created_by',
            'declined_at',
            'group',
            'ref_id',
            'to_email',
        )


class InvitationCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Invitation
        fields = (
            'group',
            'created_by',
            'to_email',
        )

    def validate_to_email(self, value):
        to_email = value
        group_id = self.initial_data['group']

        if to_email == self.context['request']._request.user.email:
            raise serializers.ValidationError(
                _("How can you invite yourself? :)")
            )

        # already invited
        # qs = models.Invitation.objects.filter(
        #     to_email=to_email, group_id=group_id
        # )
        # if qs.count():
        #     raise serializers.ValidationError(
        #         _("An invitation was already sent to this address")
        #     )

        # already a member
        qs = models.Membership.objects.filter(
            group_id=group_id,
            account__user__email=to_email,
        )
        if qs.count():
            # already a member but pending
            if qs[0].approved_at is None:
                raise serializers.ValidationError(
                    _("This person has already requested to join the group. "
                      "The admin should approve his request")
                )

            raise serializers.ValidationError(
                _("This person is already a group member")
            )
        return to_email


class ProposalRideCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.ProposalRide
        fields = (
            'available_seats',
            'created_by',
            'currency',
            'features',
            'group',
            'make',
            'price',
            'starts_from',
            'type',
        )


class ChoiceField(serializers.ChoiceField):
    """ Used to display choice fields as a dict

    { "id": "actual choice set in the database",
      "label": "what to display to the user Model.field.get_field_display" }
    """
    def to_representation(self, value):
        if value not in self.choices:
            return None

        return {
            "id": value,
            "label": str(self.choices.get(value)),
        }


class ProposalRideSerializer(serializers.ModelSerializer):
    starts_from = CitySerializer()
    currency = CurrencySerializer()
    make = ChoiceField(models.ProposalRide.MAKES)
    type = ChoiceField(models.ProposalRide.TYPES)
    features = serializers.ListField(
        child=ChoiceField(models.ProposalRide.FEATURES)
    )
    created_by = AccountSerializer()
    group = GroupSerializer()

    class Meta:
        model = models.ProposalRide
        fields = (
            'available_seats',
            'created_at',
            'created_by',
            'currency',
            'downvotes',
            'estimated_cost',
            'estimated_cost_eur',
            'features',
            'group_id',
            'group',
            'id',
            'make',
            'price',
            'starts_from',
            'type',
            'upvotes',
            'url_delete',
            'url_vote',
            'votes_score',
        )


class ProposalStayCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.ProposalStay
        fields = (
            'available_spots',
            'created_by',
            'currency',
            'group',
            'price',
            'url',
            'url_data'
        )


class ProposalStaySerializer(serializers.ModelSerializer):
    created_by = AccountSerializer()
    currency = CurrencySerializer()
    group = GroupSerializer()

    class Meta:
        model = models.ProposalStay
        fields = (
            'available_spots',
            'created_at',
            'created_by',
            'currency',
            'downvotes',
            'estimated_cost',
            'estimated_cost_eur',
            'group_id',
            'group',
            'id',
            'price',
            'upvotes',
            'url',
            'url_data',
            'url_delete',
            'url_vote',
            'votes_score',
        )


class MembershipCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Membership
        fields = (
            'group',
            'account',
        )


class MembershipGroupSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Group
        fields = ('id', 'created_by_id', 'name', 'slug', )


class MembershipSerializer(serializers.ModelSerializer):
    group = MembershipGroupSerializer()
    account = AccountSerializer()

    class Meta:
        model = models.Membership
        fields = (
            'account',
            'approved_at',
            'created_at',
            'group',
            'id',
            'inviter_id',
            'url_approve',
            'url_decline',
        )


class ContentTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = ContentType
        fields = '__all__'


class ActionGenericField(serializers.Field):
    def to_representation(self, value):
        if isinstance(value, ContentType):
            return ContentTypeSerializer(value).data
        if isinstance(value, models.Account):
            return AccountSerializer(value).data
        if isinstance(value, models.Group):
            return GroupSerializer(value).data
        if isinstance(value, models.Membership):
            return MembershipSerializer(value).data
        if isinstance(value, models.Invitation):
            return InvitationSerializer(value).data
        if isinstance(value, models.ProposalStay):
            return ProposalStaySerializer(value).data
        if isinstance(value, models.ProposalRide):
            return ProposalRideSerializer(value).data
        return value


class ActionSerializer(serializers.ModelSerializer):
    actor = ActionGenericField()
    action_object = ActionGenericField()
    target = ActionGenericField()

    actor_content_type = ActionGenericField()
    action_object_content_type = ActionGenericField()
    target_content_type = ActionGenericField()

    class Meta:
        model = Action
        fields = (
            'id',
            'timestamp',
            'actor_content_type',
            'actor',
            'verb',
            'action_object_content_type',
            'action_object',
            'target_content_type',
            'target',
        )


class NotificationSerializer(serializers.ModelSerializer):
    actor = ActionGenericField()
    action_object = ActionGenericField()
    target = ActionGenericField()

    actor_content_type = ActionGenericField()
    action_object_content_type = ActionGenericField()
    target_content_type = ActionGenericField()

    class Meta:
        model = Notification
        fields = (
            'id',
            'timestamp',
            'actor_content_type',
            'actor',
            'verb',
            'action_object_content_type',
            'action_object',
            'target_content_type',
            'target',
            'unread',
        )
