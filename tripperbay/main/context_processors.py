import datetime

from django.conf import settings


def misc(request):
    return {
        'SITE_NAME': settings.SITE_NAME,
        'SITE_DOMAIN': settings.SITE_DOMAIN,
        'SITE_URL': settings.SITE_URL,
        'CURRENT_YEAR': datetime.datetime.now().year,
    }
