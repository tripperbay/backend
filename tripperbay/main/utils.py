import requests
import traceback
import string

from bs4 import BeautifulSoup
from hashids import Hashids
from user_agent import generate_user_agent

from django.conf import settings
from django.template import TemplateDoesNotExist
from django.core.mail import EmailMessage, EmailMultiAlternatives
from django.template.loader import render_to_string

from tripperbay.main.logger import logger
from tripperbay.utils import get_fixer_rates


class Unsplash(object):
    APP_NAME = 'tb'
    CLIENT_ID = '05886dfb18407e101d6d06018bed3c01a6aca986a3a09b59a3f337beb25e3c53'
    BASE_URL = 'https://api.unsplash.com'

    L = 'landscape'
    P = 'portrait'
    S = 'squarish'
    ORIENTATIONS = (L, P, S)

    def build_url(self, path, query, orientation):
        """ Builds url with common query strings """
        orientation = orientation or self.L
        return (f'{self.BASE_URL}{path}?client_id={self.CLIENT_ID}'
                f'&query={query}&orientation={orientation}')

    def get_random(self, query, featured='true', count=1, orientation=None,
                   width=None, height=None):
        path = '/photos/random'

        url = requests.utils.requote_uri(
            self.build_url(
                path, query, orientation
            ) + f'&featured={featured}&count={count}'
        )

        try:
            logger.debug(f'Getting unsplash random image {url}')
            data = requests.get(url).json()
        except Exception as exc:
            logger.error(exc)
            return None

        images = []
        for item in data[:count]:
            src = item['urls']['raw']
            if width:
                src += '&w={}'.format(width)
            if height:
                src += '&h={}'.format(height)

            images.append({
                'src': src,
                'alt': item['description'],
                'owner_name': item['user']['name'],
                'owner_link': '{}?utm_source={}&utm_medium=referral'.format(
                    item['user']['links']['html'], self.APP_NAME
                ),
                'unsplash_link': 'https://unsplash.com/?utm_source={}&utm_medium=referral'.format(self.APP_NAME)
            })
        return images


unsplash = Unsplash()


def _unsplash(query, orientation=None, width=None, height=None, count=1):
    if orientation is None:
        orientation = 'landscape'

    allowed_orientations = ['landscape', 'portrait', 'squarish']

    if orientation not in allowed_orientations:
        raise Exception(f'Invalid orientation {orientation}. '
                        f'Allowed: {allowed_orientatios}')

    url = (f'https://api.unsplash.com/search/photos/?client_id={UNSPLASH_CLIENT_ID}&'
           f'orientation={orientation}&query={query}')

    try:
        data = requests.get(url).json()
        data = data['results']
    except Exception as exc:
        logger.error(exc)
        return []

    images = []
    for item in data[:count]:
        src = item['urls']['raw']
        if width:
            src += '&w={}'.format(width)
        if height:
            src += '&h={}'.format(height)

        images.append({
            'src': src,
            'alt': item['description'],
            'owner_name': item['user']['name'],
            'owner_link': '{}?utm_source={}&utm_medium=referral'.format(item['user']['links']['html'], UNSPLASH_APP_NAME),
            'unsplash_link': 'https://unsplash.com/?utm_source={}&utm_medium=referral'.format(UNSPLASH_APP_NAME)
        })
    return images


def hashid_compute(*ids, **kwargs):
    """
    Based on provided ids and salt returns a str representing a hashid.

    Args:
        *ids: id(s) used as base for generating a hashid
        **kwargs:
            salt (str): the salt used for hashid
            alphabet (str): the alphabet used for hashid
            length (str): the length of the hashid

    Returns (str):
        The hashid for *ids

    """
    salt = kwargs.get('salt', '')
    alphabet = kwargs.get('alphabet', string.ascii_lowercase + string.digits)
    length = kwargs.get('length', 6)

    hashids = Hashids(salt=salt, alphabet=alphabet, min_length=length)
    return hashids.encode(*ids)


def hashid_unveil(hashid, salt=None, alphabet=None):
    """ Based on hashid and salt returns a tuple of ids initially used to
    compute the provided hashid.
    """
    salt = salt or ''
    alphabet = alphabet or string.ascii_lowercase + string.digits

    hashids = Hashids(salt=salt, alphabet=alphabet)
    return hashids.decode(hashid)


def send_email(template_prefix, context, to_emails, from_email=None):
    """ Sends out emails. The subject and body are being rendered from the
    templates computed based on ``template_prefix``

    {template_prefix}_subject.txt -> for subject

    {template_prefix}_message.txt -> for body
    or
    {template_prefix}_message.html -> for body

    """
    subject = render_to_string(f'{template_prefix}_subject.txt', context)
    # remove superfluous line breaks
    subject = ' '.join(subject.splitlines()).strip()

    subject = f'{settings.EMAIL_SUBJECT_PREFIX}{subject}'

    from_email = from_email or settings.DEFAULT_FROM_EMAIL

    bodies = {}

    for ext in ['html', 'txt']:
        try:
            template_name = f'{template_prefix}_message.{ext}'
            bodies[ext] = render_to_string(template_name, context).strip()
        except TemplateDoesNotExist:
            if ext == 'txt' and not bodies:
                # We need at least one body
                raise
    if 'txt' in bodies:
        msg = EmailMultiAlternatives(subject,
                                     bodies['txt'],
                                     from_email, to_emails)
        if 'html' in bodies:
            msg.attach_alternative(bodies['html'], 'text/html')
    else:
        msg = EmailMessage(subject,
                           bodies['html'],
                           from_email, to_emails)
        msg.content_subtype = 'html'  # Main content is now text/html

    msg.send()


def get_exchange_rates(base=None):
    """ Returns a dict with exchange rates for the given base

        "rates": {
            "EUR": 1,  # base
            "USD": 1.107315,
            "RON": 4.780061
        }

    """
    EUR = 'EUR'
    base = base or EUR

    rates = get_fixer_rates()
    if base == EUR:
        return rates['rates']

    if base not in rates['rates']:
        raise Exception(f'Invalid base. Allowed: rates["rates"]')

    # changing base

    _rates = {base: 1}

    eur_to_base = rates['rates'][base]
    base_to_eur = 1 / eur_to_base

    for currency_id, value in rates['rates'].items():
        if currency_id == EUR:
            _rates[currency_id] = base_to_eur
        else:
            _rates[currency_id] = base_to_eur * rates['rates'][currency_id]

    return _rates


def currency_convert(amount, currency_id, base=None):
    """ Convers ``amount`` ``currency`` to ``base``
    """
    rates = get_exchange_rates(base=base)
    return round(amount / rates[currency_id], 2)


class ParseHeadInvalidUrl(Exception):
    """ Raised when parsing meta data for invalid urls
    eg: http://to-some-file.jpg
    """


def parse_head(url):
    headers = {
        'User-Agent': generate_user_agent()
    }

    try:
        resp = requests.get(url, headers=headers)
    except Exception as exc:
        logger.debug(f'{exc}')
        raise ParseHeadInvalidUrl

    soup = BeautifulSoup(resp.content, 'html.parser')
    if soup.html is None:
        raise ParseHeadInvalidUrl

    def get_title():
        # fb og
        tag = soup.html.head.find('meta', property='og:title')
        if tag:
            return tag.attrs['content']

        # twitter
        tag = soup.html.head.select_one("[name='twitter:title']")
        if tag:
            return tag.attrs['content']

        # regular head title
        if soup.html.head.title:
            return soup.html.head.title.text

        return ''

    def get_description():
        # fb og
        tag = soup.html.head.find('meta', property='og:description')
        if tag:
            return tag.attrs['content']

        # twitter
        tag = soup.html.head.select_one("[name='twitter:description']")
        if tag:
            return tag.attrs['content']

        # regular head description
        tag = soup.html.head.select_one("[name='description']")
        if tag:
            return tag.attrs['content']

        return ''

    def get_image_src():
        # fb og
        tag = soup.html.head.find('meta', property='og:image')
        if tag:
            return tag.attrs['content']

        # twitter
        tag = soup.html.head.select_one("[name='twitter:image']")
        if tag:
            return tag.attrs['content']

        # first image on page
        tag = soup.select_one("img")
        if tag:
            return tag.attrs['src']

        return ''

    try:
        data = {
            'title': get_title(),
            'description': get_description(),
            'image_src': get_image_src(),
        }
    except Exception:
        logger.error(traceback.format_exc())
        data = {}
    return data
