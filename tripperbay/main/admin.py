from django.contrib import admin

from . import models


class InterestAdmin(admin.ModelAdmin):
    list_display = ('name', 'is_active', )


admin.site.register(models.Interest, InterestAdmin)
admin.site.register(models.SavedSearch)
admin.site.register(models.Group)
admin.site.register(models.Account)
admin.site.register(models.Currency)
