import traceback

from channels.generic.websocket import AsyncJsonWebsocketConsumer
from channels.layers import get_channel_layer

from tripperbay.main.logger import logger

ALL = 'all'
ALL_AUTHED = 'all_authenticated'


class WebsocketConsumer(AsyncJsonWebsocketConsumer):
    async def websocket_connect(self, message):
        """ Computes self.groups based on request user account
        """
        if not self.scope['user'].is_authenticated:
            self.groups = [ALL]
        else:
            self.groups = [
                f"account_{self.scope['user'].account.id}",
                ALL,
                ALL_AUTHED,
            ]
        try:
            await super().websocket_connect(message)
        except Exception:
            logger.error(traceback.format_exc())

    async def connect(self):
        await self.accept()

    async def disconnect(self, code):
        pass

    async def receive_json(self, event):
        try:
            await websocket_broadcast(**event)
        except Exception:
            logger.error(traceback.format_exc())

    async def room_send(self, event):
        await self.send_json(event['data'])


async def websocket_broadcast(data, account_ids=None,
                              mutation=None, action=None,
                              namespace=None, consumer_method=None):
    if mutation is None and action is None:
        raise Exception('Must provide either action or mutation')

    if mutation is not None and action is not None:
        raise Exception('Must provide either action or mutation')

    consumer_method = consumer_method or 'room_send'

    if account_ids is None:
        rooms = [ALL]
    else:
        rooms = [f'account_{id}' for id in account_ids]

    channel_layer = get_channel_layer()

    payload = {
        'type': consumer_method,
        'data': {
            'namespace': namespace,
            'data': data,
        }
    }
    if mutation:
        payload['data']['mutation'] = mutation
    elif action:
        payload['data']['action'] = action

    for room in rooms:
        await channel_layer.group_send(room, payload)
