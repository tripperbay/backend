from django.conf import settings
from django.conf.urls import url
from django.contrib import admin
from django.urls import path, include
from django.views.generic import TemplateView


class Template(TemplateView):
    """ Used to test template layouts, eg: email templates
    Takes "name" query string as the template name and renders it
    """
    def get(self, request):
        self.template_name = request.GET.get('name')
        return super().get(request)


urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/auth/', include('tripperbay.auth.urls', 'api_auth')),
    path('api/chat/', include('tripperbay.chat.urls', 'api_chat')),
    path('api/geo/', include('tripperbay.geo.urls', 'api_geo')),
    path('api/main/', include('tripperbay.main.urls', 'api_main')),
    path('api/unsplash/', include('tripperbay.unsplash.urls', 'api_unsplash')),

    url(r'^__template/$', Template.as_view()),

    # below urls are used by django all auth and are defined
    # just to allow reverse() call in app
    url(r'^__sas/$', TemplateView.as_view(),
        name='socialaccount_signup'),
    url(r'^__ace/(?P<key>[-:\w]+)/$', TemplateView.as_view(),
        name='account_confirm_email'),
]

if settings.DEBUG:
    import debug_toolbar
    from django.conf.urls.static import static
    from django.conf.urls import include

    urlpatterns = [
        path('__debug__/', include(debug_toolbar.urls)),
    ] + urlpatterns + static(
        settings.MEDIA_URL, document_root=settings.MEDIA_ROOT
    )
