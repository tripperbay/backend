from django.conf.urls import url
from django.db import close_old_connections
from django.contrib.auth.models import AnonymousUser

from channels.auth import UserLazyObject
from channels.db import database_sync_to_async
from channels.middleware import BaseMiddleware
from channels.routing import ProtocolTypeRouter, URLRouter
from channels.sessions import CookieMiddleware

from rest_framework.authtoken.models import Token

from tripperbay import consumers

websocket_urlpatterns = [
    url(r'^ws/$', consumers.WebsocketConsumer),
]


@database_sync_to_async
def get_user(scope):
    key = scope['cookies'].get('auth._token.local')
    user = AnonymousUser()

    if key is None:
        return user

    key = key.replace('Token%20', '')
    try:
        token = Token.objects.get(key=key)
        user = token.user

        # hit the DB in this sync context to cache the account on the user
        # so consumers wont have to deal with SynchronousOnlyOperations
        user.account

        # close old database connections to prevent
        # usage of timed out connections
        close_old_connections()
    except Token.DoesNotExist:
        pass

    return user


class AuthTokenMiddleware(BaseMiddleware):
    """ Custom auth middleware based on rest_framework.authtoken
    """
    def populate_scope(self, scope):
        # Make sure we have cookiez
        if "cookies" not in scope:
            raise ValueError("We want cookiez!")

        # Add it to the scope if it's not there already
        if "user" not in scope:
            scope["user"] = UserLazyObject()

    async def resolve_scope(self, scope):
        scope["user"]._wrapped = await get_user(scope)

    def __call__(self, scope):
        try:
            return super().__call__(scope)
        except Exception:
            import traceback
            traceback.print_exc()


application = ProtocolTypeRouter({
    'websocket': CookieMiddleware(AuthTokenMiddleware(
        URLRouter(websocket_urlpatterns)))
})
