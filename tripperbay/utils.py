import requests


# TODO memorize decorator to cache this and remove first return below
def get_fixer_rates():
    """ Returns a dict:
    {
        "success": true,
        "timestamp": 1576069146,
        "base": "EUR",
        "date": "2019-12-11",
        "rates": {
            "USD": 1.107315,
            "RON": 4.780061
        }

    }
    """

    resp = {
        "success": True,
        "timestamp": 1576069146,
        "base": "EUR",
        "date": "2019-12-11",
        "rates": {
            "USD": 1.107315,
            "RON": 4.780061
        }

    }
    resp['rates']['EUR'] = 1
    return resp

    access_key = 'd7e079e5a3ec456b6966a0c3e83756f0'
    symbols = 'USD,RON'

    url = 'http://data.fixer.io/api/latest'
    url += f'?access_key={access_key}&symbols={symbols}'

    response = requests.get(url)
    return response.json()
