from django.conf.urls import url
from django.urls import path, include

from tripperbay.auth import views

app_name = 'tripperbay.auth'

urlpatterns = [
    path('', include('rest_auth.urls')),

    url(r'^facebook/$',
        views.FacebookLogin.as_view(), name='fb_login'),

    url(r'^google/$',
        views.GoogleLogin.as_view(), name='g_login'),

    path('registration/', include('rest_auth.registration.urls')),
]
