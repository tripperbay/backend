from django.conf import settings

from allauth.account.adapter import DefaultAccountAdapter
from allauth.socialaccount.providers.facebook.views import FacebookOAuth2Adapter  # noqa
from allauth.socialaccount.providers.google.views import GoogleOAuth2Adapter
from allauth.socialaccount.providers.oauth2.client import OAuth2Client

from rest_auth.registration.views import SocialLoginView


class AccountAdapter(DefaultAccountAdapter):
    def send_confirmation_mail(self, request, emailconfirmation, signup):
        key = emailconfirmation.key
        activate_url = f'{settings.SITE_URL}/account/confirm-email/{key}'
        ctx = {
            'user': emailconfirmation.email_address.user,
            'activate_url': activate_url,
            'SITE_URL': settings.SITE_URL,
        }
        if signup:
            email_template = 'auth/email/confirmation_signup'
        else:
            email_template = 'auth/email/confirmation'

        self.send_mail(email_template,
                       emailconfirmation.email_address.email,
                       ctx)


class FacebookLogin(SocialLoginView):
    adapter_class = FacebookOAuth2Adapter
    client_class = OAuth2Client


class GoogleLogin(SocialLoginView):
    adapter_class = GoogleOAuth2Adapter
    client_class = OAuth2Client
