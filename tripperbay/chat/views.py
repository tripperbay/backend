import operator
import time

from functools import reduce

from django.db.models import Q, Max, F
from django.shortcuts import get_object_or_404

from rest_framework import generics, status
from rest_framework.permissions import IsAuthenticated, BasePermission
from rest_framework.response import Response

from tripperbay.chat import serializers, models
from tripperbay.chat.logger import logger
from tripperbay.main.views import RefIdObject
from tripperbay.main.models import Account, Group
from tripperbay.main.serializers import AccountSerializer


class BelongsToRoom(BasePermission):
    def has_object_permission(self, request, view, obj):
        """ Checks request user account belongs to the room object
        ``obj`` can be either a message or room instance
        """
        try:
            # message instance
            room = obj.room
        except AttributeError:
            # room instance
            room = obj

        has_permission = request.user.account in room.accounts.all()

        if not has_permission:
            logger.error(f'{view}: {request.user.account} does not belong to '
                         f'{room}')
        return has_permission


class RoomCreateView(generics.CreateAPIView):
    permission_classes = (IsAuthenticated, )
    queryset = models.Room.objects.all()
    serializer_class = serializers.RoomCreateSerializer

    def get_room_data_for(self, other_account):
        """ Returns a serialized Room if the two accounts have a chat room
        or a raw dict emulating a Room without a DB id
        """
        pair = (other_account.id, self.request.user.account.id)
        for room in other_account.room_set.filter(group=None):
            this_pair = room.accounts.values_list('id', flat=True)

            match = len(set(this_pair).intersection(set(pair))) == 2
            if match:
                return serializers.RoomSerializer(instance=room).data

        accounts = [other_account, self.request.user.account]
        return {
            'id': str(time.time()),
            'group': None,
            'accounts': [AccountSerializer(instance=account).data
                         for account in accounts],

            'count_unread': 0,
            'last_message': None,
            # below are updated in gem_chat room_create ajax success
            'url_message_list': None,
            'url_room_enter': None,
        }

    def get(self, request):
        data = []

        q = self.request.query_params.get('q')
        if not q:
            return Response(data)

        # search through all members from all groups
        # where request user is a member
        groups = self.request.user.account.group_set.filter(
            membership__approved_at__isnull=False
        )

        # search for approved members
        accounts = Account.objects.filter(
            group__id__in=groups.values_list('id'),
        ).distinct().filter(reduce(operator.or_, (
            Q(user__username__icontains=q),
            Q(user__first_name__icontains=q),
            Q(user__last_name__icontains=q)
        ))).prefetch_related('room_set')

        for account in accounts:
            if account.id == request.user.account.id:
                # wont allow user to send messages to itself
                continue

            room = self.get_room_data_for(account)
            data.append(room)

        # search for groups
        for group in groups.filter(name__icontains=q):
            # a group should always have a chat room as a result of
            # the post save signal of the group (chat.signals.room_create)
            try:
                room = serializers.RoomSerializer(group.chat_room).data
            except models.Room.DoesNotExist:
                continue

            data.append(room)

        # normalize for autocomplete
        response = []
        for room in data:
            if room['group']:
                text = room['group']['name']
            else:
                for account in room['accounts']:
                    if account['id'] != self.request.user.account.id:
                        text = account['user']['get_full_name']
                        break
            response.append({
                'text': text,
                'value': room,
            })

        return Response(response)

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        self.perform_create(serializer)

        data = serializers.RoomSerializer(instance=serializer.instance).data

        return Response(data, status=status.HTTP_201_CREATED)


class RoomListView(generics.ListAPIView):
    """ Lists chat rooms for request user
    """
    permission_classes = (IsAuthenticated, )
    serializer_class = serializers.RoomSerializer

    def get_queryset(self):
        # rooms with latest message go first to be shown on top
        qs = self.request.user.account.room_set.annotate(
            latest__message=Max('message__created_at')
        ).order_by(
            F('latest__message').desc(nulls_last=True)
        )
        return qs

    def get_paginated_response(self, data):
        """ Adds a count unread for every room
        """
        response = super(RoomListView, self).get_paginated_response(data)

        results = {room['id']: room for room in response.data['results']}

        qs = self.paginator.page.object_list

        for room in qs:
            _count_unread = room.message_set.exclude(
                created_by=self.request.user.account,
            ).exclude(
                seen_by=self.request.user.account,
            ).count()

            if room.id in results:
                results[room.id]['count_unread'] = _count_unread

        response.data['results'] = results.values()

        return response


class RoomDetailView(RefIdObject, generics.RetrieveAPIView):
    permission_classes = (IsAuthenticated, BelongsToRoom)
    serializer_class = serializers.RoomSerializer
    queryset = models.Room.objects.all()

    def get_object(self):
        obj = self.get_object_by_ref_id(model_class=models.Room)
        self.check_object_permissions(self.request, obj)
        return obj

    def get(self, request, **kws):
        room = self.get_object()

        data = self.serializer_class(room).data
        data['count_unread'] = room.message_set.exclude(
            created_by=self.request.user.account,
        ).exclude(
            seen_by=self.request.user.account
        ).count()

        return Response(data)


class RoomSeenView(RefIdObject, generics.GenericAPIView):
    permission_classes = (IsAuthenticated, BelongsToRoom)
    queryset = models.Room.objects.all()

    def get_object(self):
        obj = self.get_object_by_ref_id(model_class=models.Room)
        self.check_object_permissions(self.request, obj)
        return obj

    def patch(self, request, **kws):
        """ Marks the messages from this room as seen by request user
        """
        room = self.get_object()

        unreads = room.message_set.exclude(
            created_by=request.user.account
        ).exclude(seen_by=request.user.account)

        data = []
        for message in unreads:
            message.seen_by.add(request.user.account)
            data.append(serializers.MessageSerializer(message).data)

        return Response(data, status=status.HTTP_200_OK)


class RoomMessageListView(RefIdObject, generics.ListAPIView):
    """ Lists messages from a given room
    """
    permission_classes = (IsAuthenticated, BelongsToRoom)
    serializer_class = serializers.MessageSerializer

    def get_object(self):
        obj = self.get_object_by_ref_id(model_class=models.Room)
        self.check_object_permissions(self.request, obj)
        return obj

    def get_queryset(self):
        room = self.get_object()
        return room.message_set.all()


class GroupRoomRefView(generics.RetrieveAPIView):
    permission_classes = (IsAuthenticated, BelongsToRoom)
    serializer_class = serializers.RoomSerializer
    queryset = models.Room.objects.all()

    def get_object(self):
        group = get_object_or_404(Group, id=self.kwargs['group_id'])

        room = group.chat_room

        self.check_object_permissions(self.request, room)
        return room

    def get(self, request, **kws):
        room = self.get_object()
        return Response(room.ref_id)


class MessageCountUnreadView(generics.GenericAPIView):
    permission_classes = (IsAuthenticated, )

    def get(self, request):
        count_unread = 0

        for room in request.user.account.room_set.all():
            count = room.message_set.exclude(
                created_by=request.user.account
            ).exclude(
                seen_by=request.user.account
            ).count()

            count_unread += count

        return Response(count_unread)


class MessageCreateView(generics.CreateAPIView):
    # NOTE room permission is checked during serializer validation
    permission_classes = (IsAuthenticated, )
    queryset = models.Message.objects.all()
    serializer_class = serializers.MessageCreateSerializer

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        self.perform_create(serializer)

        message = serializer.instance

        data = serializers.MessageSerializer(instance=message).data

        return Response(data, status=status.HTTP_201_CREATED)


class MessageSeenView(generics.GenericAPIView):
    permission_classes = (IsAuthenticated, BelongsToRoom)
    queryset = models.Message.objects.all()

    def patch(self, request, **kws):
        """ Marks the message as seen by request user
        """
        message = self.get_object()
        message.seen_by.add(request.user.account)
        data = serializers.MessageSerializer(message).data
        return Response(data, status=status.HTTP_200_OK)
