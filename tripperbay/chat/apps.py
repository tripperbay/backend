from django.apps import AppConfig


class ChatConfig(AppConfig):
    name = 'tripperbay.chat'

    def ready(self):
        import tripperbay.chat.signals  # noqa

        from actstream import registry

        registry.register(
            self.get_model('Message'),
        )
