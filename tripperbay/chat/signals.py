from django.db.models import signals
from django.dispatch import receiver

from tripperbay.chat.logger import logger
from tripperbay.chat.models import Room, Message
from tripperbay.main.models import Group, Membership


# NOTE lets keep this naming scheme for signal funcs
# entity_verb or entity_attribute_verb


@receiver(signals.post_save, sender=Group)
def room_create(sender, instance, created, **kws):
    """ Creates a Room for the Group
    """
    try:
        instance.chat_room
    except Room.DoesNotExist:
        room = Room(group=instance)
        room.save()
        logger.debug(f'Created Room(id={room.id}) for Group(id={instance.id})')


# @receiver(signals.post_save, sender=Message)
# def message_seen_by_created_by(sender, instance, created, **kws):
#     if not created:
#         return
#
#     instance.seen_by.add(instance.created_by)


@receiver(signals.post_save, sender=Membership)
def account_add_to_room(sender, instance, created, **kws):
    """ Adds the approved member to the corresponding Room
    """
    if not instance.approved_at:
        # member added but not yet approved by admin
        return

    instance.group.chat_room.accounts.add(instance.account)

    logger.debug(f'Added Account(id={instance.account.id}) to '
                 f'Room(id={instance.group.chat_room.id}) because its '
                 f'membership was approved in Group(id={instance.group.id})')
