from django.db import models
from django.urls import reverse

from tripperbay.main.models import (
    Account, Group, RefIdTimestampModel, TimestampModel
)


class RoomManager(models.Manager):
    def get_queryset(self):
        qs = super(RoomManager, self).get_queryset()

        return qs.select_related('group').prefetch_related(
            'accounts',
            # 'accounts__user',
            'message_set',
            'message_set__seen_by',
            # 'message_set__seen_by__user',
        )


class Room(RefIdTimestampModel):
    objects = RoomManager()
    accounts = models.ManyToManyField(Account)
    group = models.OneToOneField(
        Group,  models.CASCADE, blank=True, null=True,
        related_name='chat_room'
    )

    @property
    def url_message_list(self):
        return reverse('chat:room_message_list', args=(self.ref_id, ))

    @property
    def url_room_seen(self):
        return reverse('chat:room_seen', args=(self.ref_id, ))

    @property
    def count_unread(self):
        """ Dummy slot which gets recomputed based on request user account
        in views
        """
        return 0

    @property
    def last_message(self):
        """ Returns last message sent in the room
        """
        qs = self.message_set.all()
        if qs.count():
            return qs[0]

        # there is no message when the room is first created
        return None


class MessageManager(models.Manager):
    def get_queryset(self):
        qs = super(MessageManager, self).get_queryset()
        return qs.select_related(
            'created_by', 'room'
        ).prefetch_related('seen_by')


class Message(TimestampModel):
    objects = MessageManager()

    room = models.ForeignKey(Room, models.CASCADE)
    created_by = models.ForeignKey(Account, models.CASCADE)
    reply_to = models.ForeignKey('self', models.CASCADE, null=True, blank=True)

    text = models.CharField(max_length=512)

    is_hidden = models.BooleanField(default=False)

    seen_by = models.ManyToManyField(Account, related_name='messageseen_set')

    class Meta:
        ordering = ('-created_at', )

    @property
    def url_message_seen(self):
        return reverse('chat:message_seen', args=(self.id, ))
