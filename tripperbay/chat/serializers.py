from rest_framework import serializers

from tripperbay.main.serializers import AccountSerializer, GroupSerializer
from tripperbay.chat import models


class MessageCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Message
        fields = ('room', 'text', 'reply_to', 'created_by')

    def validate_room(self, room):
        if self.context['request'].user.account in room.accounts.all():
            return room
        raise serializers.ValidationError("You don't belong to this room")


class MessageSerializer(serializers.ModelSerializer):
    created_by = AccountSerializer(read_only=True)
    seen_by = AccountSerializer(many=True, read_only=True)

    class Meta:
        model = models.Message
        fields = (
            'created_at',
            'created_by',
            'id',
            'is_hidden',
            'room_id',
            'seen_by',
            'text',
            'url_message_seen',
        )


class RoomCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Room
        fields = ('accounts', 'group',)


class RoomSerializer(serializers.ModelSerializer):
    accounts = AccountSerializer(many=True, read_only=True)
    group = GroupSerializer(read_only=True)
    last_message = MessageSerializer(read_only=True)

    class Meta:
        model = models.Room
        fields = (
            'accounts',
            'count_unread',
            'group',
            'id',
            'last_message',
            'url_message_list',
            'url_room_seen',
            'ref_id',
        )
