from django.conf.urls import url

from . import views

app_name = 'chat'

urlpatterns = [
    url(r'^room/create/$',
        views.RoomCreateView.as_view(), name='room_create'),
    url(r'^room/list/$',
        views.RoomListView.as_view(), name='room_list'),
    url(r'^room/(?P<ref_id>\w+)/$',
        views.RoomDetailView.as_view(), name='room_detail'),
    url(r'^room/(?P<ref_id>\w+)/seen/$',
        views.RoomSeenView.as_view(), name='room_seen'),
    url(r'^room/(?P<ref_id>\w+)/message/list/$',
        views.RoomMessageListView.as_view(), name='room_message_list'),

    url(r'^group/(?P<group_id>\d+)/room/ref/$',
        views.GroupRoomRefView.as_view(), name='group_room_ref'),

    url(r'^message/count/unread/$',
        views.MessageCountUnreadView.as_view(), name='message_count_unread'),
    url(r'^message/create/$',
        views.MessageCreateView.as_view(), name='message_create'),
    url(r'^message/(?P<pk>\d+)/seen/$',
        views.MessageSeenView.as_view(), name='message_seen'),
]
