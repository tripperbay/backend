from django.contrib import admin

from tripperbay.unsplash import models

admin.site.register(models.Collection)
