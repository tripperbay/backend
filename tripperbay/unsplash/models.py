import requests

from django.contrib.postgres.fields import ArrayField, JSONField
from django.db import models

from tripperbay.main.logger import logger


class Unsplash(object):
    APP_NAME = 'tb'
    CLIENT_ID = '05886dfb18407e101d6d06018bed3c01a6aca986a3a09b59a3f337beb25e3c53'
    BASE_URL = 'https://api.unsplash.com'

    L = 'landscape'
    P = 'portrait'
    S = 'squarish'

    ORIENTATIONS = (L, P, S)

    def fetch(self, path):
        """ Adds the base url and client id to path and fetches response
        """
        if '?' in path:
            path = f'{path}&client_id={self.CLIENT_ID}'
        else:
            path = f'{path}?client_id={self.CLIENT_ID}'

        url = requests.utils.requote_uri(f'{self.BASE_URL}{path}')
        try:
            logger.debug(f'Fetching {url}')
            return requests.get(url).json()
        except Exception as exc:
            logger.error(exc)

        return None

    def get_collection_photos(self, collection_id, page=1, per_page=10):
        """
        https://unsplash.com/documentation#get-a-collections-photos
        """
        query_string = f'?page={page}&per_page={per_page}'
        path = f'/collections/{collection_id}/photos{query_string}'

        return self.fetch(path)

    def get_photo(self, unsplash_id):
        path = f'/photos/{unsplash_id}'
        return self.fetch(path)

    def get_random_photo(self, query, count=None, orientation=None):
        count = count or 1
        orientation = orientation or self.L

        path = f'/photos/random?featured=true'
        path += f'&count={count}&orientation={orientation}&query={query}'

        return self.fetch(path)

    def search_photo(self, query, orientation=None, page=1, per_page=10):
        orientation = orientation or self.L
        path = f'/search/photos?orientation={orientation}'
        path += f'&query={query}'
        path += f'&page={page}&per_page={per_page}'

        return self.fetch(path)


unsplash = Unsplash()  # TODO move out ??


class UnsplashAbstractModel(models.Model):
    """ Abstract model used by other apps like main, geo, etc.

    Placed here to avoid any circular imports (eg: between main and geo models)
    """
    images = ArrayField(JSONField(), blank=True, default=list)

    class Meta:
        abstract = True

    @property
    def image(self):
        if self.images:
            return self.images[0]  # TODO cycle index
        return {
            'dummy': True,
            'urls': {
                'full': '/static/img/dummy.png',
                'regular': '/static/img/dummy.png',
                'small': '/static/img/dummy.png',
                'thumb': '/static/img/dummy.png',
            },
        }


class Collection(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=128, unique=True)

    def get_photos(self):
        return unsplash.get_collection_photos(self.id)
