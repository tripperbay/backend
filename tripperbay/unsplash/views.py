import random

from rest_framework import generics, status
from rest_framework.permissions import IsAuthenticated, BasePermission
from rest_framework.response import Response

from tripperbay.unsplash import models, serializers


class CollectionListView(generics.ListAPIView):
    queryset = models.Collection.objects.all()
    serializer_class = serializers.CollectionSerializer


class CollectionImageListView(generics.GenericAPIView):
    queryset = models.Collection.objects.all()

    def get(self, request, **kws):
        try:
            obj = models.Collection.objects.get(name=kws['slug'])
        except models.Collection.DoesNotExist:
            return Response(status=status.HTTP_400_BAD_REQUEST)

        photos = obj.get_photos()

        random.shuffle(photos)

        return Response(photos)


class PhotoView(generics.GenericAPIView):
    serializer_class = serializers.IdSerlizer

    def get_queryset(self):
        return None

    def post(self, request, **kws):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        data = models.unsplash.get_photo(serializer.data['id'])

        data.pop('tags', None)
        data.pop('related_collections', None)

        return Response(data)


class PhotoRandomView(generics.GenericAPIView):
    serializer_class = serializers.QuerySerlizer

    def get_queryset(self):
        return None

    def post(self, request, **kws):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        data = models.unsplash.get_random_photo(serializer.data['query'])

        return Response(data)
