from rest_framework import serializers
from tripperbay.unsplash import models


class CollectionSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Collection
        fields = ('id', 'name', )


class IdSerlizer(serializers.Serializer):
    id = serializers.CharField()


class QuerySerlizer(serializers.Serializer):
    query = serializers.CharField()
