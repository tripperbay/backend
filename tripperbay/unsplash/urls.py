from django.conf.urls import url

from tripperbay.unsplash import views

app_name = 'unsplash'

urlpatterns = [
    url(r'^collection/list/$',
        views.CollectionListView.as_view(), name='collection_list'),
    url(r'^collection/(?P<slug>[\w-]+)/$',
        views.CollectionImageListView.as_view(), name='collection_images'),
    url(r'^photo/$',
        views.PhotoView.as_view(), name='photo'),
    url(r'^photo/random/$',
        views.PhotoRandomView.as_view(), name='photo_random'),
]
