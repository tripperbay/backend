FROM python:3.8.0-alpine

RUN apk update && apk add postgresql-dev gcc python3-dev musl-dev libffi-dev zlib-dev jpeg-dev && \
    mkdir -p /tripperbay/backend && \
    mkdir -p /tripperbay/media/static && \
    mkdir -p /tripperbay/media/uploads && \
    mkdir -p /var/log/tripperbay

WORKDIR /tripperbay/backend

COPY . /tripperbay/backend

RUN pip install --upgrade pip && pip install -r requirements.txt

# Patch actstream and notifications
COPY ./patch/actstream/actions.py /usr/local/lib/python3.8/site-packages/actstream/
COPY ./patch/actstream/models.py /usr/local/lib/python3.8/site-packages/actstream/
COPY ./patch/actstream/registry.py /usr/local/lib/python3.8/site-packages/actstream/
COPY ./patch/notifications/models.py /usr/local/lib/python3.8/site-packages/notifications/

RUN cp ./tripperbay/settings/local_example.py ./tripperbay/settings/local.py
