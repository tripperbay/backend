# Changelog

## Version 0.2

- Added tripperbay apps: chat (beta), geo, main
- Integrated authentication: tripperbay.auth, rest_auth, allauth

## Version 0.1

- Initial commit
