# TripperBay Django Backend

TripperBay Backend based on Django, Django Rest Framework and Django Channels

### Quickstart

Install the dependencies, export the environment variables and start at http://127.0.0.1:8000

    pip install -r requirements.txt
    cp tripperbay/settings/local_example.py tripperbay/settings/local.py
    export $(cat example.env | xargs)
    ./manage.py runserver

### Patch actstream and notifications

    cp ./patch/actstream/actions.py ~/.virtualenvs/tripperbay/lib/python3.7/site-packages/actstream/
    cp ./patch/actstream/models.py ~/.virtualenvs/tripperbay/lib/python3.7/site-packages/actstream/
    cp ./patch/actstream/registry.py ~/.virtualenvs/tripperbay/lib/python3.7/site-packages/actstream/
    cp ./patch/notifications/models.py ~/.virtualenvs/tripperbay/lib/python3.7/site-packages/notifications/

### Create log folder

    sudo mkdir -p /var/log/tripperbay
    sudo chown $USER /var/log/tripperbay


### Run databases

Run Postgresql and Redis as docker containers:

    docker-compose -f ../docker-compose-local-dbs.yml up -d


### Fixtures

Some fixtures might be needed, for the fresh builds:

    export $(cat example.env | xargs)
    ./manage.py migrate --noinput
    ./manage.py collectstatic --noinput
    ./manage.py loaddata fixtures/currencies.json
    ./manage.py loaddata fixtures/geo.json
    ./manage.py loaddata fixtures/interests.json
    ./manage.py loaddata fixtures/sites.json
    ./manage.py loaddata fixtures/socialapps.json
    ./manage.py loaddata fixtures/unsplash.json
